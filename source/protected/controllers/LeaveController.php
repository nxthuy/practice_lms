<?php

class LeaveController extends Controller
{

    public $_leaveModel;
    public $modelClass = 'Leave';

    public function actionIndex()
    {
        if (Yii::app()->user->isGuest) {
            $this->redirect('/user/login');
        }

        if ($_POST) {
            $leave = $this->loadLeaveModel();
            if ($_POST['action'] == 'approve') {
                $leave->status = Leaves::STATUS_APPROVED;
            } elseif ($_POST['action'] == 'reject') {
                $leave->status = Leaves::STATUS_REJECTED;
            }

            $leave->notes = $_POST['message'];
            $leave->save();
        }

        // $leaveModel = $this->loadLeaveModel();

        /*
                $criteria = new CDbCriteria();
                $criteria->order = 'id ASC';
                $count = Leave::model()->count($criteria);
                $pages = new CPagination(Leave::model()->count());
                $pages->pageSize = 5;
                $pages->applyLimit($criteria);
                $leave = Leave::model()->findAll($criteria);
                $dataProvider = new CActiveDataProvider('leave');

                $this->render('index', array(
                    'leaveModel' => $leaveModel,
                    'dataProvider' => $dataProvider,
                    'pages' => $pages,
                    'leave' => $leave,

                ));*/
        $this->render('index', array(//  'leaveModel' => $leaveModel,
        ));
    }

    public function actionCreate()
    {
        if (Yii::app()->user->isGuest) {
            $this->redirect('/user/login');
        }


        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($baseUrl.'/css/listleavestyle.css');

        $model = new Leave('createleave');
        $modelSearch = new Leave('search');
        $modelSearch->unsetAttributes();

        if (isset($_GET['Leave'])) {
            $modelSearch->attributes = $_GET['Leave'];

        }
        $userProfile = UserProfile::model()->findByAttributes(array(
            'user_id' => Yii::app()->user->getId()
        ));
        $categoryLeave = Category::model()->findAll(array('select' => 'id, name', 'order' => 'name'));
        $approvingOfficer = User::model()->with(array(
            'userRole' => array(
                'select' => false,
                'joinType' => 'INNER JOIN',
                'condition' => 'userRole.user_role_id IN(1,2) AND userRole.user_status=1 AND t.user_status=1',
            ),
        ))->findAll();
        $urgentPhone = $userProfile->user_phone;


        // collect user input data
        if (isset($_POST['Leave'])) {
            $model->attributes = $_POST['Leave'];
            $model->requester_id = Yii::app()->user->id;
            $modelCate = Category::model()->findByAttributes(array('id' => $model->category_id));
            $fromTime = date_create($model->from_time);
            $toTime = date_create($model->to_time);
            $model->from_time = date_format($fromTime, 'Y-m-d H:i:s');
            $model->to_time = date_format($toTime, 'Y-m-d H:i:s');
            // count days exclude weekend(saturday & sunday)
            $model->applied_days = $this->countDay($fromTime, $toTime);

            $firstNameRequester = $userProfile->user_first_name;
            $lastNameRequester = $userProfile->user_last_name;
            $nameRequester = $firstNameRequester . ' ' . $lastNameRequester;

            $userProfileApprover = UserProfile::model()->findByAttributes(array('user_id' => $model->approver_id));
            $firstNameApprover = $userProfileApprover->user_first_name;
            $lastNameApprover = $userProfileApprover->user_last_name;
            $nameApprover = $firstNameApprover . ' ' . $lastNameApprover;

            $modelUser = User::model()->findByAttributes(array('user_id' => $model->approver_id));
            $emailApprover = $modelUser->user_email;
            $model->status = Leave::STATUS_PENDING;
            $model->created_at = date('Y-m-d H:i:s', time());
            $model->four_digit_year = CTimestamp::get4DigitYear(date('y', strtotime($model->from_time)));
            //validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                if ($model->save()) {
                    $params = array(
                        'data' => array(
                            'requesterName' => $nameRequester,
                            'urgentPhone' => $model->urgent_phone,
                            'leaveType' => $modelCate->name,
                            'time' => $model->from_time,
                            'workingDaysApplied' => $model->applied_days,
                            'approvingOfficer' => $nameApprover,
                            'addressWhileOnLeave' => $model->wol_address,
                            'remark' => $model->reason,
                        ),
                        'to' => array($nameApprover, $emailApprover),
                        'cc' => $model->cc_email,

                    );

                    CeresMail::queue('leaveRecord', $params, EmailQueue::TYPE_URGENT);

                    Yii::app()->user->setFlash('createLeave', 'Your leave has been sent to system. We will respond to you as soon as possible.');
                    $this->refresh();
                }
            }
        }

        $criteria = new CDbCriteria();
        $count = Leave::model()->count($criteria);
        $pages = new CPagination($count);
//        $pages->pageSize = 4;
//        if (isset($_GET['pageSize']) && (int)$_GET['pageSize']) {
//            $pages->pageSize = $_GET['pageSize'];
//        }
        $pages->applyLimit($criteria);
        $leaves = Leave::model()->findAll($criteria);

        $this->render('createLeave', array(
            'userProfile' => $userProfile,
            'model' => $model,
            'modelSearch' => $modelSearch,
            //'filter' => $filter,
            'categoryLeave' => $categoryLeave,
            'approvingOfficer' => $approvingOfficer,
            'leaves' => $leaves,
            'pages' => $pages,
            'urgentPhone' => $urgentPhone,

        ));

    }

    public function actionGridView()
    {
        $model = new Leave('search');
        $modelLeave = new Leave();
        $model->unsetAttributes();

        if (isset($_GET['Leave'])) {
            $model->attributes = $_GET['Leave'];
        }
        if (!isset($_GET['ajax'])) {
            $this->render('_listLeaveRecord', array(
                'modelSearch' => $model,
                'modelLeave' => $modelLeave,
            ));
        } else {
            $this->renderPartial('_listLeaveRecord', array(
                'modelSearch' => $model
            ));
        }
    }

    protected function countDay($fromTime, $toTime)
    {
//        if (strtotime(date_format($fromTime, 'Y-m-d H:i:s')) >= strtotime(date_format($toTime, 'Y-m-d H:i:s'))) {
//            return $count = 0;
//        } else {
            $count = 1;
            while ($fromTime->diff($toTime)->days > 0) {
                $count += $fromTime->format('N') < 6 ? 1 : 0;
                $fromTime = $fromTime->add(new \DateInterval("P1D"));
            }
            return $count;
//        }
    }


    public function actionView()
    {
        $model = new Leave();
        if (isset($_POST['ajaxData'])) {
            $model = new Leave();
            $id = $_POST['ajaxData'];
            $modelLeave = $model->findByAttributes(array('id' => $id));

            $this->renderPartial('_viewLeave', array(
                'modelLeave' => $modelLeave
            ));
        }


    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionApprove()
    {

        if (Yii::app()->request->isPostRequest) {
            $leave = $this->loadLeaveModel();
            $leave->approve();
            $this->redirect("/employee");
        }
    }

    public function actionReject()
    {
        if (Yii::app()->request->isPostRequest) {
            $leave = $this->loadLeaveModel();
            $leave->reject();
            $this->redirect('/employee');
        }
    }

    public function actionDelete()
    {

    }


    function loadLeaveModel()
    {
        if ($this->_leaveModel === null) {
            if (isset($_GET['id'])) {
                $this->_leaveModel = Leave::model()->findByPk($_GET['id']);
            }
            if ($this->_leaveModel === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
        return $this->_leaveModel;
    }
    /*
    function loadModel($id)
    {
        $model = Leave::model()->findByPk($id);
        if($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;

    }
    function getLeaveOption()
    {
        $leaveoption =  CHtml::listData(Leave::model()->findAll(),'category_id','id');
        return $leaveoption;
    }
*/

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
