<?php

class EmployeeController extends Controller {
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('index', 'edit', 'addmembers', 'addallmembers', 'removemember', 'removeallmembers'),
                'expression' => array($this, 'isAdmin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($id = null) {
        if (Yii::app()->user->isGuest) {
            $this->redirect('user/login');
        }

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($baseUrl.'/css/listleavestyle.css');

        $model = null;
        if ($id != null) {
            $model = User::model()->findByPk($id);
        } else {
            $model = User::model()->findByPk(user()->id);
        }

        //sort
        $sort = 'user_role_id ASC';
        if (isset($_POST['sort'])) {
            $sort = $_POST['sort'];
        }

        // get user count
        $userCriteria = new CDbCriteria(array(
            'order' => $sort,
            'with' => 'userProfiles'
        ));
        $userCriteria->addCondition('user_status="Active"');
        $page = Yii::app()->request->getParam('page', 1);
//        $userCount = User::model()->count($userCriteria);

        // Pagination
//        $userPages = new CPagination($userCount);
        $pageSize = app()->getParams()['pageSize'];
        if (app()->request->isAjaxRequest) {
            if (app()->request->getParam('ajaxPageSize') != NULL) {
                $pageSize = app()->request->getParam('ajaxPageSize');
            }
        }
        $dataProvider = new CActiveDataProvider(
                'User',
                array(
                    'criteria' => $userCriteria,
                    'pagination' => array(
                        'pageSize' => $pageSize,
                    ),
                )
        );

//        echo "<pre>";
//        print_r(count($dataProvider->data));
//        echo "</pre>";
//        exit();

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => $model,
            //'userPages' => $userPages,
            //'userCount' => $userCount,
            ));
    }

    //add Employee
    public function actionAddUser() {
        $userModel = new User;
        $userModel->setScenario('addEmployerUser');
        $this->performAjaxValidation($userModel);
        $listForRoleNameDropDown = CHtml::listData(UserRole::model()->findAll(), 'user_role_id', 'user_role_name');
        $listForUserStatusDropDown = CHtml::listData(User::model()->findAll(), 'user_status', 'user_status');

        if (isset($_POST['User'])) {
            $userModel->setAttributes($_POST['User']);
            $userModel->user_status = 'Active';

            if ($userModel->save()) {
                $userProfile = new UserProfile();
                $userProfile->setScenario('addEmployerUser');
                $userProfile->user_id = $userModel->user_id;
                $userProfile->user_dob = null;
                $userProfile->user_photo = 'avatar2.jpg';

                if ($userProfile->save()) {
                    Yii::app()->user->setFlash('success', 'New User has been added.');
                    $this->refresh();
                }
            }
        }

        $this->render('addUser', array(
            'userModel' => $userModel,
            'roleList' => $listForRoleNameDropDown,
            'statusList' => $listForUserStatusDropDown
        ));
    }

    protected function performAjaxValidation($userModel) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'addUser-form') {
            echo CActiveForm::validate($userModel);
            Yii::app()->end();
        }
    }

    public function actionEdit($id) {
        Yii::import('application.vendor.imageResize.*');
        require_once('ImageResize.php');

        //Yii::app()->theme='admin';
        $oldImage = '';

//        $user = User::model()->findByPk(Yii::app()->user->getId());
//        $userprofile = $user->profile;
        $userProfile = UserProfile::model()->with('user')->findByAttributes(array(
            'user_id' => $id,
        ));


        if ($userProfile->user_photo != 'photo.png') {
            $oldImage = $userProfile->user_photo;
        }

        $userProfile->setScenario('profileUpdate');

        if (isset($_POST['UserProfile'])) {
            $userProfile->attributes = $_POST['UserProfile'];
            $userProfile->user_gender = $_POST['UserProfile']['user_gender'];
            $userSignatureFile = CUploadedFile::getInstance($userProfile, 'uploadedFile');
            $userImageUploadFile = CUploadedFile::getInstance($userProfile, 'userImage');

            if (!empty($userSignatureFile)) {
                $basePath = dirname(Yii::app()->request->scriptFile) . '/uploads/';
                $rnd = md5('BDUOIU*&*(=-0_)' . rand(0, 999999) . 'TNKKOU**(');
                $fileName = "{$rnd}.{$userSignatureFile->extensionName}";

                if ($userSignatureFile->saveAs($basePath . $fileName)) {
                    $userProfile->user_signature_name = $userSignatureFile->name;
                    $userProfile->user_signature_type = $userSignatureFile->type;

                    $imageResize = new ImageResize();
                    $imageResize->setBasePath($basePath);
                    $imageResize->load($fileName);
                    $imageResize->resize(UserProfile::SIGNATURE_IMAGE_HEIGHT, UserProfile::SIGNATURE_IMAGE_WIDTH);
                    $imageResize->setBasePath($basePath);
                    $imageResize->save($fileName);
                    $userProfile->user_signature_size = $imageResize->getImageSizeInByte();
                    $userProfile->user_signature = base64_encode(file_get_contents($basePath . $fileName));
                    if (file_exists($basePath . $fileName))
                        unlink($basePath . $fileName);
                }

            }
            if (!empty($userImageUploadFile)) {
                $rnd = rand(0, 9999);
                $fileName = "{$userProfile->user->user_name}-{$rnd}.{$userImageUploadFile->extensionName}";  // random number + file name
                $userProfile->user_photo = $fileName;

                $basePath = dirname(Yii::app()->request->scriptFile) . '/images/user/';

                Yii::app()->user->setState('userImage', $userProfile->user_photo);
                $userImageUploadFile->saveAs($basePath . $userProfile->user_photo);
                if (!empty($oldImage)) {
                    if (file_exists($basePath . $oldImage))
                        unlink($basePath . $oldImage);
                }
                $imageResize = new ImageResize();
                $imageResize->setBasePath($basePath);
                $imageResize->load($userProfile->user_photo);
                $imageResize->resize(UserProfile::IMAGE_HEIGHT, UserProfile::IMAGE_WIDTH);
                $imageResize->setBasePath($basePath);
                $imageResize->save($userProfile->user_photo);
                $userProfile->save(false);
            }

            if ($userProfile->save()) {
                Yii::app()->user->setFlash('success', 'Your Profile has been updated.');
            }
        }
        $user = User::model()->findAllByAttributes(array(
            'user_role_id' => User::ROLE_GENERAL,
        ));

        $this->render('profile', array('model' => $userProfile));
    }

    public function actionRemoveMember()
    {
        if (app()->request->isAjaxRequest) {
            $leaderId = app()->request->getParam('leader_id');
            $memberId = app()->request->getParam('member_id');

            $model = LeaderMember::model()->findByAttributes(array('member_id' => $memberId));
            if ($model) {
                if ($model->delete()) {
                    LeaderMember::model()->deleteAllByAttributes(array('member_id' => $model->member_id));
                }
                $memberLeader = LeaderMember::model()->find('leader_id=:leader_id', array(':leader_id' => $leaderId));
                if ($memberLeader) {
                    echo 1;
                } else {
                    echo 2;
                }
            } else {
                echo 0;
            }
        }
    }

    public function actionRemoveAllMembers()
    {
        if (app()->request->isAjaxRequest) {
            $leader_id = app()->request->getParam('leader_id');
            $leaderMember = LeaderMember::model()->findAllByAttributes(array('leader_id' => $leader_id));
            $check = 1;

            foreach ($leaderMember as $item) {
                if ($item->delete()) {
                    LeaderMember::model()->deleteAllByAttributes(array('leader_id' => $item->leader_id));
                } else {
                    $check = 0;
                }
            }
            if ($check) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function actionAddMembers()
    {
        if (app()->request->isAjaxRequest) {
            $leaderId = app()->request->getParam('leader_id');
            $freeMemberId = app()->request->getParam('free_member_id');
            $arrFreeMember = explode(',', $freeMemberId);
            $results = array();
            for ($i=0; $i<count($arrFreeMember); $i++) {
                $leaderMember = LeaderMember::model()->findByAttributes(array('member_id' => $arrFreeMember[$i]));
                if (is_null($leaderMember)) {
                    $model = new LeaderMember();
                    $model->leader_id = $leaderId;
                    $model->member_id = $arrFreeMember[$i];
                    $model->save();

                    $results[$i][] = $model->member_id;
                    $user = User::model()->findByPk($model->member_id);
                    $results[$i][] = $user->user_name;

                }
            }
            echo CJSON::encode($results);
            app()->end();
        }
    }

    public function actionAddAllMembers()
    {
        if (app()->request->isAjaxRequest) {
            $leaderId = app()->request->getParam('leader_id');
            $freeMemberId = app()->request->getParam('free_member_id');
            $arrFreeMember = explode(',', $freeMemberId);
            $criteria = new CDbCriteria();
            foreach ($arrFreeMember as $item) {
                $criteria->addCondition('user_id=' . $item, 'OR');
            }
            $freeMember = User::model()->findAll($criteria);
            $results = array();
            $i = 0;
            foreach ($freeMember as $item) {
                $leaderMember = LeaderMember::model()->findByAttributes(array('member_id' => $item->user_id));
                if (is_null($leaderMember)) {
                    $model = new LeaderMember();
                    $model->leader_id = $leaderId;
                    $model->member_id = $item->user_id;
                    $model->save();

                    $results[$i][] = $model->member_id;
                    $user = User::model()->findByPk($model->member_id);
                    $results[$i][] = $user->user_name;
                    $i++;
                }
            }
            echo CJSON::encode($results);
            app()->end();
        }
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
