<?php

class Helpers
{

    // does the first string start with the second?
    public static function stringSplitByWords($string, $words)
    {
        $returnString = '';
        $array = explode(' ', $string);
        for($i=0; $i<$words; $i++)
        {
           $returnString.=$array[$i].' ';
        }
       return $returnString;
    }

}