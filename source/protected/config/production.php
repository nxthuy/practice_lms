<?php
return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(

        'modules'=>array(
            // uncomment the following to enable the Gii tool
            'gii'=>array(
                'class'=>'system.gii.GiiModule',
                'password'=>'123',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters'=>array('127.0.0.1','::1'),
            ),
        ),

        'components'=>array(

            'db'=>array(
                'connectionString' => 'mysql:host=localhost;dbname=lms_db',
                'emulatePrepare' => true,
                'username' => 'lms',
                'password' => 'qUyB6LsGRXKRnMWE',
                'charset' => 'utf8',
                'enableProfiling'=>true,
                'enableParamLogging' => true,
            ),

           'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error, warning, trace, info',
                        'categories'=>'system.*',
                    ),
                    array(
                        'class'=>'CEmailLogRoute',
                        'levels'=>'error,warning, trace, info',
                       'emails'=>'sajib.cse03@gmail.com',
                    ),
                ),
            ),
        ),





    )
);

?>