<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic,600italic,700italic,700,800italic,800' rel='stylesheet' type='text/css'>
    <link href="<?php echo Yii::app()->request->baseUrl;?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl;?>/css/main.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl;?>/css/calendar.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl;?>/css/color.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/vendor/html5shiv.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/vendor/respond.min.js"></script>
    <![endif]-->
    <!--[if lte IE 7]><script src="<?php echo Yii::app()->request->baseUrl;?>/js/vendor/lte-ie7.js"></script><![endif]-->
</head>
<body id="login">
<div class="login">
    <h1 class="logo clearfix">
        <a href="/site/index"><img alt="" src="<?php echo Yii::app()->request->baseUrl;?>/img/logo.png"><span>CERES LMS</span></a>
    </h1>

    <?php
    $flashMessages = Yii::app()->user->getFlashes();
    if ($flashMessages) {
        echo '<ul class="flashes">';
        foreach($flashMessages as $key => $message) {
            echo '<li><div class="alert alert-' . $key . '" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <strong>' . $message . "</strong></div></li>\n";
        }
        echo '</ul>';
    }
    ?>

    <?php echo $content;?>
</div><!--End login-->

<!-- END COPYRIGHT -->
<script src="<?php echo Yii::app()->request->baseUrl;?>/js/vendor/bootstrap.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl;?>/js/vendor/bootstrap-datepicker.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl;?>/js/application.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl;?>/js/main.js"></script>
</body>
</html>
