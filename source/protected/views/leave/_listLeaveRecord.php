<?php
/*
$nowYear = CTimestamp::get4DigitYear(date('y',time()));

$dataProvider=new CActiveDataProvider('Leave', array(
    'criteria'=>array(
        'condition'=>'four_digit_year='.$nowYear.' and requester_id='.Yii::app()->user->id,
        'order'=>'created_at DESC',
    ),
    'pagination'=>array(
        'pageSize'=>5,
    ),
));
*/


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'listLeave',
    'enablePagination' => true,
    'enableSorting' => true,
    'dataProvider' => $modelSearch->search(),
    //'afterAjaxUpdate' => 'reinstallDatePicker',
    'filter' =>$modelSearch,
    'htmlOptions' => array(
        'style' => 'text-align: center; color: #303030;',
        'class' => 'table-responsive clearfix top-40',
    ),
    'itemsCssClass' => 'table table-hover table-striped table-bordered',
    'template' => '{items}<div class="config dropdown pull-right">
										<a class="tfoot-btn" data-toggle="dropdown" href="#">
										<span class="glyphicon glyphicon-cog"></span></a>
										<ul class="dropdown-menu">
											<li><a href="#">Show up to:</a></li>
											<li><a href="#" class="page_size" id="5">5 items</a></li>
											<li><a href="#" class="page_size" id="10">10 items</a></li>
											<li><a href="#" class="page_size" id="15">15 items</a></li>
											<li><a href="#" class="page_size" id="20">20 items</a></li>
										</ul>
									</div>{pager}{summary}',
    'pager' => array(
        'header' => '',//text before it
        'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',//overwrite nextPage lable
        'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',//overwrite prePage lable

        'internalPageCssClass' => 'hidden',
        'nextPageCssClass' => 'pager_next',
        'previousPageCssClass' => 'pager_prev'
    ),

    'pagerCssClass' => 'pull-right arrow tfoot-btn',
    'summaryText' =>'{start}-{end} Of {count}',
    'summaryCssClass' => 'pull-right summary',
    'columns' => array(
        array(
            'name' => 'id',
            'header' => 'No.',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-03',
            ),
            'filter' => '',
        ),

        array(
            'name' => 'category_id',
            'header'=>'Leave Type <span class="glyphicon glyphicon-chevron-down"></span>',
            'filter' => CHtml::activeDropDownList(
                $modelSearch,
                'category_id',
                $modelSearch->listCategories
            ),
            'value' => '$data->category->name',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-012',
            )
        ),
        array(
            'name' => 'from_time',
            'header' => 'From <span class="glyphicon glyphicon-chevron-down"></span>',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-1',
            ),
            'value' => '$data->getFromDate()',
        ),
        array(
            'name' => 'to_time',
            'header' => 'To <span class="glyphicon glyphicon-chevron-down"></span>',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-1',
            ),
            'value' => '$data->getToDate()',
        ),
        array(
            'name' => 'applied_days',
            'header' => 'No. Days <span class="glyphicon glyphicon-chevron-down"></span>',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-1',
            )
        ),
        array(
            'name' => 'cc_email',
            'header' => 'CC to other <span class="glyphicon glyphicon-chevron-down"></span>',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-012',
            )
        ),
        array(
            'name' => 'wol_address',
            'header' => 'Address <span class="glyphicon glyphicon-chevron-down"></span>',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-012',
            )
        ),
        array(
            'name' => 'userprofile_search',
            'header' => 'A. Officer <span class="glyphicon glyphicon-chevron-down"></span>',
            'value' => '$data->getNameApprover()',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-1',
            )
    ),
        array(
            'name'=>'reason',
            'header' => 'Remark <span class="glyphicon glyphicon-chevron-down"></span>',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-2',
            )
        ),
        array (
            'class'=>'CButtonColumn',
            'header' => 'Action',
            'template' => '<a rel="tooltip" title="" data-id="$data->id" class="view_button" data-toggle="modal" href="#view" data-original-title="View"><span class="glyphicon glyphicon-eye-open"></span></a>',
            'headerHtmlOptions' => array(
                'class' => 'col-xs-03'
            )
        ),

    ),
));
?>
<!-- Modal View-->
<div class="modal fade modal-view" id="view">

</div><!-- /.modal -->

<script>
    $(document).on('click', '.view_button', function(){
        var id = $(this).closest('tr').find('td:first').html();
        $.ajax({

                url: '/leave/view',
                type: 'POST',
                data: {ajaxData: id},
                success: function(data){
                    $('.modal-view').html(data);
                }
            });

    });

    $(document).on('click', '.page_size', function(){
        var pageSize = $(this).attr('id');
        $.fn.yiiGridView.update('listLeave', {
            url: $(this).attr('href'),
            data: {ajaxPageSize: pageSize}
        });
        return false;
    });
</script>


