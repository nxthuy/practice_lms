<head>
    <title>Submit Your Leave Record</title>
</head>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'htmlOptions' => array(
        'class' => 'form-horizontal col-lg-6',
        'role' => 'form'
    ),

));
?>
    <?php if (Yii::app()->user->hasFlash('createLeave')): ?>
        <div class="alert alert-success">
            <?php echo Yii::app()->user->getFlash('createLeave'); ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
    <?php endif; ?>

<div class="form-group">
    <?php echo $form->labelEx($model, 'category_id', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-7">
        <?php echo $form->dropDownList($model, 'category_id', CHtml::listData($categoryLeave, 'id', 'name')); ?>
    </div>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'from_time', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-3">
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'from_time',
            'model' => $model,
            'attribute' => 'from_time',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => 'dd-mm-yy',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => date('Y') . ':' . (date('Y') + 1)
            ),
            'htmlOptions' => array(
                //'style' => 'height:35px; width:123px'
                'class' => 'datepicker form-control',
                'data-date-format' => 'dd/mm/yyyy'
            )
        ));
        ?>
        <?php //echo $form->dateField($model,'from_time',array('class'=>'datepicker form-control')); ?>
        <?php echo $form->error($model, 'from_time'); ?>
    </div>

    <?php echo $form->labelEx($model, 'to_time', array('class' => 'col-lg-1 control-label')); ?>
    <div class="col-lg-3">
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'to_time',
            'model' => $model,
            'attribute' => 'to_time',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => 'dd-mm-yy',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => date('Y') . ':' . (date('Y') + 1)
            ),
            'htmlOptions' => array(
                //'style' => 'height:35px; width:123px'
                'class' => 'datepicker form-control',
                'data-date-format' => 'dd/mm/yyyy'
            )
        ));
        ?>
        <?php //echo $form->dateField($model,'to_time',array('class'=>'datepicker form-control', 'data-date-format' => 'dd/mm/yyyy'));  ?>
        <?php echo $form->error($model, 'to_time'); ?>
    </div>
</div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'applied_days', array('class' => 'col-lg-4 control-label', 'for' => '')); ?>
        <div class="col-lg-7">
            <?php
            echo $form->textField($model, 'applied_days', array(
                'class' => 'form-control',
                'disabled' => 'disabled',
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'applied_days'); ?>
    </div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'approver_id', array('class' => 'col-lg-4 control-label', 'for' => '')); ?>
    <div class="col-lg-7">
        <?php echo $form->dropDownList($model, 'approver_id', CHtml::listData($approvingOfficer, 'user_id', 'user_name')); ?>
    </div>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'cc_email', array('class' => 'col-lg-4 control-label', 'for' => '')); ?>
    <div class="col-lg-7">
        <?php
        echo $form->textField($model, 'cc_email', array(
            'class' => 'form-control',
            'placeholder' => 'Add email to cc'
        ));
        ?>
    </div>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'wol_address', array('class' => 'col-lg-4 control-label', 'for' => '')); ?>
    <div class="col-lg-7">
        <?php echo $form->textArea($model, 'wol_address'); ?>
    </div>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'urgent_phone', array('class' => 'col-lg-4 control-label', 'for' => '')); ?>
    <div class="col-lg-7">
        <?php
        echo $form->textField($model, 'urgent_phone', array(
            'class' => 'form-control',
            'value' => $urgentPhone
        ));
        ?>
    </div>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'reason', array('class' => 'col-lg-4 control-label', 'for' => '')); ?>
    <div class="col-lg-7">
        <?php echo $form->textArea($model, 'reason', array('maxlength' => 200)); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-offset-4 col-lg-6">
        <?php echo CHtml::submitButton('Submit', array('class' => 'btn-success')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn-default')); ?>
    </div>
</div>    

<?php $this->endWidget(); ?>
<?php
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/leave/createLeave.js', CClientScript::POS_END);
?>
