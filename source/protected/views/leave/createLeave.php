<section class="col-lg-12">
<h1 class="title">Leave Record for <?php if(isset($userProfile)) echo ucfirst($userProfile->user_first_name)." ".ucfirst($userProfile->user_last_name)?></h1>
<div class="row clearfix">
    <?php $this->renderPartial('_createLeaveForm', array(
        'model' => $model,
        'categoryLeave' => $categoryLeave,
        'approvingOfficer' => $approvingOfficer,
        'urgentPhone' => $urgentPhone,
    )); ?>

    <div class="col-lg-6">
        <div class="note"><strong>*Note:</strong>
            <ul>
                <li>Personal leave is only allowed to be taken after Annual leave is run out </li>
                <li>Member enter leave record must enter exactly date time. </li>
                <li>Do not enter non-pay leave information here. </li>
                <li>Smalest unit can be 0.25 </li>
            </ul>
        </div>
        <div class="table-responsive top-10">
            <table class="table table-chart table-hover table-striped table-bordered">
                <?php $this->widget('application.widgets.usersummary.UserSummary', array(
                    'userId' => user()->id,
                )); ?>
            </table><!--End responsive-->
        </div><!--End table-responsive-->
    </div>
</div>
<div class="clearfix"></div>
    <?php $this->renderPartial('_listLeaveRecord', array(
        'model' => $model,
        'modelSearch' => $modelSearch,
    )); ?>


<!--End table-responsive-->

</section>