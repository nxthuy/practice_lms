<?php
/* @var $this EmployeeController */
/* @var $data User */
/* @var $dataProvider EmployeeController */
/* @var $model User */
/* @var $user_id User */
/* @var $userPages EmployeeController */
/* @var $userCount EmployeeController*/

$this->breadcrumbs = array(
    'List Employees',
    //$model->user_id,
);
?>

<head>
    <title>List Employees</title>
</head>
<section class="col-lg-12">
<h1 class="title">List Employees</h1>

<div class="row clearfix">
<div class="col-lg-5">
    <div class="pull-left mail-view dropdown">
        <div>
            <a href="/employee/adduser"><button class="btn btn-success">New Employee</button></a>
        </div>
    <div class="clearfix"></div>
    <div>
    <?php
        echo CHtml::dropDownList('sort', '', array(
                'user_role_id ASC' => 'Position',
                'user_first_name ASC' => 'Name',
                ),
            array('id' => 'sortDrop',
                'options' => array(
                                    'user_role_id ASC' => array('disabled' => false, 'label' => 'position.ASC'),
                                    'user_first_name ASC' => array('label' => 'name.ASC')
                                  )
                )
        )
    ;?>

    </div>
    </div>


    <div class="clearfix"></div>
    <div id="list" class="top-20">
        <ul class="list-title">
            <li class="col-xs-1">No.</li>
            <li class="col-xs-3">Avatar</li>
            <li class="col-xs-4">Name</li>
            <li class="col-xs-3">Position</li>
            <li class="col-xs-1">Edit</li>
        </ul>
        <?php
        $this->widget('zii.widgets.CListView', array(
            'id' => 'userListView',
            'dataProvider' => $dataProvider,
            'summaryText' => '',
            'emptyText' => 'No data to display!',
            'itemView' => '_viewEmployee',
            'viewData' => array('currentEmployeeId' => $model->user_id),
            'htmlOptions' => array(
                'style' => 'text-align: center; color: #303030;',
            ),
            'itemsCssClass' => 'table table-hover table-striped table-bordered',
            'template' => '{items}<div class="config dropdown pull-right">
										<a class="tfoot-btn" data-toggle="dropdown" href="#">
										<span class="glyphicon glyphicon-cog"></span></a>
										<ul class="dropdown-menu">
											<li><a href="#">Show up to:</a></li>
											<li><a href="#" class="page_size" id="5">5 items</a></li>
											<li><a href="#" class="page_size" id="10">10 items</a></li>
											<li><a href="#" class="page_size" id="15">15 items</a></li>
											<li><a href="#" class="page_size" id="20">20 items</a></li>
										</ul>
									</div>{pager}{summary}',
            'pager' => array(
                'header' => '',//text before it
                'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',//overwrite nextPage lable
                'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',//overwrite prePage lable
                'internalPageCssClass' => 'hidden',
                'nextPageCssClass' => 'pager_next',
                'previousPageCssClass' => 'pager_prev'
            ),

            'pagerCssClass' => 'pull-right arrow tfoot-btn',
            'summaryText' =>'{start}-{end} Of {count}',
            'summaryCssClass' => 'pull-right summary',
        )); ?>
    </div>
</div>
<div class="tab-content col-lg-7">

<?php if($model != null) { ?>
<div class="tab-pane active" id="view">
    <h2><strong><?php echo $model->userProfiles['user_first_name']; ?></strong> Details </h2>

    <div class="row clearfix">
        <div class="col-lg-10 row">
            <div class="row-small">

                <span class="col-lg-4 field-name">First name:</span>
                <span class="col-lg-8"><?php echo $model->userProfiles['user_first_name']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-lg-4 field-name">Last name:</span>
                <span class="col-lg-8"><?php echo $model->userProfiles['user_last_name']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-lg-4 field-name">Gender:</span>
                <span class="col-lg-8"><?php echo $model->userProfiles['user_gender']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-lg-4 field-name">Birthday:</span>
                <span class="col-lg-8"><?php echo date('d/m/Y', strtotime($model->userProfiles['user_birthday'])); ?></span>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="">
                <img class="avatar" alt="" src="<?php echo (!empty($model->userProfiles->user_photo) ? '/images/user/' . $model->userProfiles->user_photo : '/images/no_avatar.jpg'); ?>">
            </div>
        </div>

        <div class="col-lg-10 row">
            <div class="row-small">
                <span class="col-lg-4 field-name">Email:</span>
                <span class="col-lg-8"><?php echo $model->user_email; ?></span>
            </div>
            <div class="row-small">
                <span class="col-lg-4 field-name">Phone:</span>
                <span class="col-lg-8"><?php echo CHtml::encode($model->userProfiles['user_phone']);?></span>
            </div>
            <div class="row-small">
                <span class="col-lg-4 field-name">Address:</span>
                <span class="col-lg-8"><?php echo $model->userProfiles['user_address'];?></span>
            </div>
            <div class="row-small">
                <span class="col-lg-4 field-name">Position:</span>
                <span class="col-lg-8"><?php echo $model->getRoleName();?></span>
            </div>
        </div>
    </div>
    <div class="table-responsive clearfix top-20">
        <table class="table table-chart table-hover table-striped table-bordered">
            <?php
            if (!isset($_GET['id'])) {
                $this->widget('application.widgets.usersummary.UserSummary', array(
                    'userId' => user()->id,
                ));
            } else {
                $this->widget('application.widgets.usersummary.UserSummary', array(
                    'userId' => $_GET['id'],
                ));
            }
            ?>
        </table>
        <!--End responsive-->
    </div>
    <!--End table-responsive-->
</div>
<?php } ?>

</div>
</div>
</section>

<?php
Yii::app()->clientScript->registerScript('specialSort', '$("#sortDrop").on("change",
                    function(){
                                    $.fn.yiiListView.update("userListView",
                                                            {data:{sort:$(this).val()},
                                                            type:"POST"
                                                            })
                               }
                )
    '
);
?>

<script>
    $(document).on('click', '.page_size', function(){
        var pageSize = $(this).attr('id');
        $.fn.yiiListView.update('userListView', {
            url: $(this).attr('href'),
            data: {ajaxPageSize: pageSize}
        });
        return false;
    });
</script>