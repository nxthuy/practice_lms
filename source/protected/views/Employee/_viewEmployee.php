<?php
/* @var $data User */
/* @var $model EmployeeController */
//var_dump($data->userRole);die();
?>

<ul class="nav nav-tabs">
    <li class="<?php echo $data->user_id == $currentEmployeeId ? 'active' : ''; ?>" id="<?php echo $data->user_id; ?>">
        <a href="<?php echo Yii::app()->controller->createUrl("employee/index", array("id" => $data->user_id)); ?>" data-toggle="<?php echo $currentEmployeeId == $data->user_id ? 'tab' : ''; ?>">
            <span class="col-xs-1 t-center"><?php echo $data->user_id;?></span>
            <span class="col-xs-3"><img class="avatar-small" src="<?php echo '/images/user/' . CHtml::encode($data->userProfiles['user_photo']) ?>" alt=""></span>
            <span class="col-xs-4"><?php echo CHtml::encode($data->userProfiles['user_first_name']) . " " . CHtml::encode($data->userProfiles['user_last_name']); ?></span>
            <span class="col-xs-3"><?php echo $data->getRoleName(); ?></span>
        </a>
        <a href="<?php echo Yii::app()->controller->createUrl("employee/edit", array("id" => $data->user_id)); ?>">
            <span class="col-xs-1 glyphicon glyphicon-edit" style="padding-top: 12px;"></span>
        </a>
    </li>
</ul>

