<?php
$url = Yii::app()->Controller->createUrl('/user/user/detail?id='.$data->requester_id);
$uRProfile = $data->userRequest->userProfiles;
;?>
<div class="latest-leave">
    <a href="<?php echo $url;?>"><span class="item"><?php echo CHtml::encode($uRProfile->user_first_name.' '.$uRProfile->user_last_name);?></span></a> has
    <span class="item"><?php echo $data->applied_days;?></span> days from
    <span class="item"><?php echo date('d/m/Y', strtotime($data->from_time));?></span> to
    <span class="item"><?php echo date('d/m/Y', strtotime($data->to_time));?></span>.
    <span class="created-time"><?php echo date('d/m/Y', strtotime($data->created_at));?></span>
</div>
