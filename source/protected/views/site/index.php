<?php
/* @var $this SiteController */
/* @var $dataProvider SiteController */
/* @var $pages SiteController */
/* @var $leaveCount SiteController */
?>

<head>
    <title>Dashboard</title>
</head>
<section class="col-xs-6">
    <h2>Latest Leaves</h2>
    <div class="clearfix"></div>
        <?php
        $this->widget('zii.widgets.CListView',
            array(
                'id' => 'latest-leaves',
                'dataProvider' => $dataProviderLatest,
                'enablePagination' => false,
                'summaryText' => '',
                'emptyText' => 'No data to display!',
                'itemView' => '_viewLatestLeaves',
                'template' => '{items}',
            )
        );
        ?>
</section>
<section class="col-xs-6">
    <h2><?php echo $pendingCount > 0 ? "You have " . $pendingCount . " Leave Request" . ( $pendingCount > 1 ? 's' : '') . " pending." : "Great! You've read all the messages in your inbox.";?></h2>
    <div class="col-lg-2">
        <a href="/leave/create"><button class="btn btn-success">New Leave Record</button></a>
    </div>
    <div class="pull-left mail-view dropdown view-all">
        <a data-toggle="dropdown" href="#">View:

            <span>All</span>
            <span class="glyphicon glyphicon-chevron-down"></span>
        </a>
        <ul class="dropdown-menu">
            <li><a href="<?php echo Yii::app()->controller->createUrl("site/index");?>">All</a></li>
            <li><a href="<?php echo Yii::app()->controller->createUrl("site/sortpending");?>">Pending</a></li>

        </ul>
    </div>
    <div class="clearfix"></div>
    <div id="leaveMessage" class="table-responsive top-20">
        <table class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th class="col-xs-1">Avatar</th>
                <th class="col-xs-2">Sender</th>
                <th class="col-xs-8">Remark</th>
                <th class="col-xs-1">Status</th>
                <th class="col-xs-1">Date</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $this->widget('zii.widgets.CListView',
                array(
                    'id' => 'leaveMessage',
                    'dataProvider' => $dataProvider,
                    'enablePagination' => true,
                    'summaryText' => '',
                    'emptyText' => 'No data to display!',
                    'itemView' => '_viewLeaveMessage',
                    'htmlOptions' => array(
                        'style' => 'text-align: center; color: #303030;',
                    ),
                    'itemsCssClass' => 'table table-hover table-striped table-bordered',
                    'template' => '{items}<div class="config dropdown pull-right">
										<a class="tfoot-btn" data-toggle="dropdown" href="#">
										<span class="glyphicon glyphicon-cog"></span></a>
										<ul class="dropdown-menu">
											<li><a href="#">Show up to:</a></li>
											<li><a href="#" class="page_size" id="5">5 items</a></li>
											<li><a href="#" class="page_size" id="10">10 items</a></li>
											<li><a href="#" class="page_size" id="15">15 items</a></li>
											<li><a href="#" class="page_size" id="20">20 items</a></li>
										</ul>
									</div>{pager}{summary}',
                    'pager' => array(
                        'header' => '',//text before it
                        'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',//overwrite nextPage lable
                        'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',//overwrite prePage lable

                        'internalPageCssClass' => 'hidden',
                        'nextPageCssClass' => 'pager_next',
                        'previousPageCssClass' => 'pager_prev'
                    ),

                    'pagerCssClass' => 'pull-right arrow tfoot-btn',
                    'summaryText' =>'{start}-{end} Of {count}',
                    'summaryCssClass' => 'pull-right summary',
                )
            );
            ?>
            </tbody>
        </table>
    </div>
</section>

<script>
    $(document).on('click', '.page_size', function(){
        var pageSize = $(this).attr('id');
        $.fn.yiiListView.update('leaveMessage', {
            url: $(this).attr('href'),
            data: {ajaxPageSize: pageSize}
        });
        return false;
    });
</script>