<?php
$this->pageTitle=Yii::app()->name . ' - Manage Employees';
$this->breadcrumbs=array(
    'ManageEmployees',
);
?>
<head>
    <title>Manage Employees</title>
</head>

<section class="col-xs-12">
<h1 class="title">Leave Record for Mary See</h1>
<div class="row clearfix">
    <div class="col-lg-6 row">
        <div class="row-small">
            <div class="col-xs-3">
                <img class="avatar" alt="" src="/img/avatar1.jpg">
            </div>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">First name:</span>
            <span class="col-xs-8">Mary</span>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">Last name:</span>
            <span class="col-xs-8">Lee</span>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">Gender:</span>
            <span class="col-xs-8">Women</span>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">Birthday:</span>
            <span class="col-xs-8">2/2/1980</span>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">Email:</span>
            <span class="col-xs-8">marylee@ceresolutions.com</span>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">Phone:</span>
            <span class="col-xs-8">0123456789</span>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">Address:</span>
            <span class="col-xs-8">Nguyen Van Qua St., D.12, Ho Chi Minh city</span>
        </div>
        <div class="row-small">
            <span class="col-xs-4 field-name">Position:</span>
            <span class="col-xs-8">Development</span>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="note"><strong>*Note:</strong>
            <ul>
                <li>Personal leave is only allowed to be taken after Annual leave is run out </li>
                <li>Member enter leave record must enter exactly date time. </li>
                <li>Donot enter non-pay leave information here. </li>
                <li>Smalest unit can be 0.25 </li>
            </ul>
        </div>
        <div class="table-responsive top-10">
            <table class="table table-chart table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Type Leave</th>
                    <th>Leave has</th>
                    <th>Leave</th>
                    <th>Left</th>
                    <th>No. of Days</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td rowspan="3">Company pay leave (days)</td>
                    <td>Annual leave</td>
                    <td class="t-center">12</td>
                    <td class="t-center">3</td>
                    <td class="t-center">9</td>
                    <td class="t-center">0</td>
                </tr>
                <tr>
                    <td>Wedding</td>
                    <td class="t-center">3</td>
                    <td class="t-center">3</td>
                    <td class="t-center">&#32;</td>
                    <td class="t-center">0</td>
                </tr>
                <tr>
                    <td>Personal</td>
                    <td class="t-center">3</td>
                    <td class="t-center">3</td>
                    <td class="t-center">&#32;</td>
                    <td class="t-center">0</td>
                </tr>
                <tr>
                    <td rowspan="2">Social insurance pay leave (70%)</td>
                    <td>Sick</td>
                    <td>&#32;</td>
                    <td>&#32;</td>
                    <td>&#32;</td>
                    <td class="t-center">3</td>
                </tr>
                <tr>
                    <td>Maternity</td>
                    <td>&#32;</td>
                    <td>&#32;</td>
                    <td>&#32;</td>
                    <td class="t-center">0</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="5">Total</td>
                    <td class="t-center">3</td>
                </tr>
                </tfoot>
            </table><!--End responsive-->
        </div><!--End table-responsive-->
    </div>
</div>
<div class="table-responsive clearfix top-40">
    <table class="table table-hover table-striped table-bordered">
        <thead>
        <tr>
            <th class="col-xs-03">No.</th>
            <th class="col-xs-012">Leave Type
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-1">From
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-1">To
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-1">No. Days
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-012">CC to other
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-012">Address
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-1">A. Officer
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-2">Remark
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </th>
            <th class="col-xs-03">Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>&#32;</td>
            <td>
                <select>
                    <option>Annual</option>
                    <option>Wedding</option>
                    <option>Sick</option>
                    <option>Personal</option>
                    <option>Maternity</option>
                </select>
            </td>
            <td><input class="datepicker" type="text"></td>
            <td><input class="datepicker" type="text"></td>
            <td><input class="" type="text"></td>
            <td><input class="" type="email"></td>
            <td><input class="" type="text"></td>
            <td><input class="" type="text"></td>
            <td><input class="" type="text"></td>
            <td>&#32;</td>
        </tr>
        <tr>
            <td class="t-center">1</td>
            <td>Annual</td>
            <td>02/05/2013</td>
            <td>03/05/2013</td>
            <td class="t-center">1</td>
            <td>abc@ceresolutions.com, acd@ceresolutions.com</td>
            <td>Nguyen Van Qua St., D.12, Ho Chi Minh city</td>
            <td>Anna Howard </td>
            <td>
                Lorem Ipsum is simply dummy text of the printing...
            </td>
            <td class="t-center">
                <a rel="tooltip" title="" class="view" data-toggle="modal" href="#view" data-original-title="View">
                    <span class="glyphicon glyphicon-eye-open"></span>
                </a>
            </td>
        </tr>
        <tr>
            <td class="t-center">1</td>
            <td>Annual</td>
            <td>02/05/2013</td>
            <td>03/05/2013</td>
            <td class="t-center">1</td>
            <td>abc@ceresolutions.com, acd@ceresolutions.com</td>
            <td>Nguyen Van Qua St., D.12, Ho Chi Minh city</td>
            <td>Peter Lee</td>
            <td>
                Lorem Ipsum is simply dummy text of the printing...
            </td>
            <td class="t-center">
                <a rel="tooltip" title="" class="view" data-toggle="modal" href="#view" data-original-title="View">
                    <span class="glyphicon glyphicon-eye-open"></span>
                </a>
            </td>
        </tr>
        <tr>
            <td class="t-center">2</td>
            <td>Sick</td>
            <td>09/04/2013</td>
            <td>12/04 2013</td>
            <td class="t-center">3</td>
            <td>abc@ceresolutions.com, acd@ceresolutions.com</td>
            <td>Nguyen Van Qua St., D.12, Ho Chi Minh city</td>
            <td>Peter Lee</td>
            <td>Lorem Ipsum is simply dummy text of the printing...</td>
            <td class="t-center">
                <a rel="tooltip" title="" class="view" data-toggle="modal" href="#view" data-original-title="View">
                    <span class="glyphicon glyphicon-eye-open"></span>
                </a>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="10">
                <div class="pull-right">
                    <span class="number-page">1 - 5 0f 40</span>
                    <div class="arrow tfoot-btn">
                        <a class="prev" href="#">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="next" href="#">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    <div class="config dropdown">
                        <a class="tfoot-btn" data-toggle="dropdown" href="#">
                            <span class="glyphicon glyphicon-cog"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Show up to:</a></li>
                            <li><a href="#">5 items</a></li>
                            <li><a href="#">10 items</a></li>
                            <li><a href="#">15 items</a></li>
                            <li><a href="#">20 items</a></li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
        </tfoot>
    </table><!--End responsive-->
</div>
<!-- Modal View-->
<div class="modal fade" id="view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="pull-left modal-title">Leave Record for <span>Mary See</span></h2>
                <div class="pull-right">
                    <ul class="nav-ul">
                        <li>
                            <a class="circle" href="#">
                                <span class="glyphicon glyphicon-print"></span>
                                <span class="text">Print</span>
                            </a>
                        </li>
                        <li>
                            <a class="circle" href="#">
                                <span class="glyphicon glyphicon-saved"></span>
                                <span class="text">Save as PDF</span>
                            </a>
                        </li>
                        <li>
                            <a class="circle" href="#" aria-hidden="true" data-dismiss="modal" type="button">
                                <span class="glyphicon glyphicon-remove"></span>
                                <span class="text">Close</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 modal-body clearfix">
                <div class="row">
                    <div class="row-small">
                        <span class="col-xs-4 field-name">Leave Type</span>
                        <span class="col-xs-8">Annual</span>
                    </div>
                    <div class="row-small">
                        <span class="col-xs-4 field-name">Time</span>
                        <span class="col-xs-8">08 Step 2013</span>
                    </div>
                    <div class="row-small">
                        <span class="col-xs-4 field-name">Working Days Applied</span>
                        <span class="col-xs-8">1</span>
                    </div>
                    <div class="row-small">
                        <span class="col-xs-4 field-name">Approving Officer</span>
                        <span class="col-xs-8">Peter Lee</span>
                    </div>
                    <div class="row-small">
                        <span class="col-xs-4 field-name">CC to other</span>
                        <span class="col-xs-8">abc@ceresolutions.com,acd@ceresolutions.com</span>
                    </div>
                    <div class="row-small">
                        <span class="col-xs-4 field-name">Address While On Leave</span>
                        <span class="col-xs-8">Nguyen Van Qua St., D.12, Ho Chi Minh city</span>
                    </div>
                    <div class="row-small">
                        <span class="col-xs-4 field-name">Remark:</span>
									<span class="col-xs-8">Lorem Ipsum is simply dummy text of the printing and typesetting
									industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </span>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</section>