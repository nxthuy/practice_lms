<?php
/* @var $data Leaves*/
$uRProfile = $data->userRequest->userProfiles;
?>

<tr>
    <td class="col-xs-1"><img class="avatar-small" alt="" src="<?php echo '/images/user/'.CHtml::encode($uRProfile->user_photo);?>"</td>
    <td class="col-xs-4"><?php echo CHtml::encode($uRProfile->user_first_name.' '.$uRProfile->user_last_name);?></td>
    <td class="col-xs-6"><?php echo CHtml::encode($data->reason) ;?></td>
    <td class="col-xs-1"><?php echo CHtml::encode($data->status == Leave::STATUS_PENDING ? 'Pending' : ($data->status == Leave::STATUS_APPROVED ? 'Approved' : 'Rejected'));?></td>
    <td class="col-xs-1"><?php echo date('d/m/Y', strtotime($data->created_at));?></td>
</tr>
