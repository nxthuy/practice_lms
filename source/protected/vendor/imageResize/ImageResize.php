<?php
/**
 * Created by PhpStorm.
 * User: sajib pc
 * Date: 19/03/14
 * Time: 22:06
 */

class ImageResize
{   
    public  $image;
    public  $image_type;
    private $basePath;
    private $filename;

    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }

    private function getFile()
    {
        return $this->basePath.$this->filename;
    }

    public function load($filename)
    {
        $this->filename = $filename;
        $image_info = getimagesize($this->getFile());
        $this->image_type = $image_info[2]; 
        
        if( $this->image_type == IMAGETYPE_JPEG ) 
        {   
            $this->image = imagecreatefromjpeg($this->getFile()); 
        } 
        elseif( $this->image_type == IMAGETYPE_GIF ) 
        {   
            $this->image = imagecreatefromgif($this->getFile()); 
        } 
        elseif( $this->image_type == IMAGETYPE_PNG ) 
        {   
            $this->image = imagecreatefrompng($this->getFile()); 
        }

        print_r($image_info);
    } 
    
    public function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null)
    {
        $this->filename = $filename;

        if( $image_type == IMAGETYPE_JPEG ) 
        { 
            imagejpeg($this->image,$this->getFile(),$compression);
        }
        elseif( $image_type == IMAGETYPE_GIF )
        {   
            imagegif($this->image,$this->getFile()); 
        } 
        elseif( $image_type == IMAGETYPE_PNG ) 
        {   
            imagepng($this->image,$this->getFile()); 
        }
        if( $permissions != null) 
        {   
            chmod($this->getFile(),$permissions); 
        } 
    } 
    
    public function output($image_type=IMAGETYPE_JPEG) 
    {   
        if( $image_type == IMAGETYPE_JPEG ) 
        { 
            imagejpeg($this->image); 
        } 
        elseif( $image_type == IMAGETYPE_GIF ) 
        {   
            imagegif($this->image); 
        }
        elseif( $image_type == IMAGETYPE_PNG ) 
        {   
            imagepng($this->image);
        } 
    } 
    
    public function getWidth() 
    {   
        return imagesx($this->image); 
    } 
    
    public function getHeight() 
    {   
        return imagesy($this->image); 
    } 
    
    public function resizeToHeight($height) 
    {   
        $ratio = $height / $this->getHeight(); $width = $this->getWidth() * $ratio; $this->resize($width,$height);
    }  
    
    public function resizeToWidth($width) 
    {
        $ratio = $width / $this->getWidth(); $height = $this->getheight() * $ratio; $this->resize($width,$height);
    }  
    
    public function scale($scale) 
    {
        $width = $this->getWidth() * $scale/100; $height = $this->getheight() * $scale/100; $this->resize($width,$height); 
    }   
    
    public function resize($width,$height) 
    { 
        $new_image = imagecreatetruecolor($width, $height); 
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight()); $this->image = $new_image; 
    }

    public function getImageSizeInByte()
    {
       return abs(filesize($this->getFile()));
    }

} 

?>