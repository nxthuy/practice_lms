<?php
/**
* This is the model class for table "email_attachments".
*
* The followings are the available columns in table 'email_attachments':
* @property integer $id
* @property integer $queue_id
* @property string $file_path
*/
class EmailAttachment extends ActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return EmailAttachment the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'email_attachments';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('queue_id, file_path', 'required'),
			array('queue_id', 'numerical', 'integerOnly' => true),
			array('file_path', 'length', 'max' => 255),
			array('id, queue_id, file_path', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
            'queue' => array(self::BELONGS_TO, 'EmailQueue', 'queue_id'),
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'queue_id' => 'Queue',
			'file_path' => 'File Path',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('queue_id', $this->queue_id);
		$criteria->compare('file_path', $this->file_path, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}