<?php
/* @var $this AlertController */
/* @var $data Alert */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->alert_id), array('view', 'id'=>$data->alert_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->alert_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_schedule_id')); ?>:</b>
	<?php echo CHtml::encode($data->alert_schedule_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_table')); ?>:</b>
	<?php echo CHtml::encode($data->alert_table); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_subject')); ?>:</b>
	<?php echo CHtml::encode($data->alert_subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_body')); ?>:</b>
	<?php echo CHtml::encode($data->alert_body); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_type')); ?>:</b>
	<?php echo CHtml::encode($data->alert_type); ?>
	<br />


</div>