<?php
/* @var $this AlertController */
/* @var $model Alert */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'alert-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_category_id'); ?>
		<?php echo $form->textField($model,'alert_category_id'); ?>
		<?php echo $form->error($model,'alert_category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_schedule_id'); ?>
		<?php echo $form->textField($model,'alert_schedule_id'); ?>
		<?php echo $form->error($model,'alert_schedule_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_table'); ?>
		<?php echo $form->textField($model,'alert_table'); ?>
		<?php echo $form->error($model,'alert_table'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_subject'); ?>
		<?php echo $form->textField($model,'alert_subject',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'alert_subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_body'); ?>
		<?php echo $form->textArea($model,'alert_body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'alert_body'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_type'); ?>
		<?php echo $form->textField($model,'alert_type',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'alert_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->