<?php
/* @var $this AlertTableFieldsController */
/* @var $model AlertTableFields */

$this->breadcrumbs=array(
	'Alert Table Fields'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AlertTableFields', 'url'=>array('index')),
	array('label'=>'Manage AlertTableFields', 'url'=>array('admin')),
);
?>

<h1>Create AlertTableFields</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>