<?php
/* @var $this AlertTableFieldsController */
/* @var $model AlertTableFields */

$this->breadcrumbs=array(
	'Alert Table Fields'=>array('index'),
	$model->alert_table_fields_id,
);

$this->menu=array(
	array('label'=>'List AlertTableFields', 'url'=>array('index')),
	array('label'=>'Create AlertTableFields', 'url'=>array('create')),
	array('label'=>'Update AlertTableFields', 'url'=>array('update', 'id'=>$model->alert_table_fields_id)),
	array('label'=>'Delete AlertTableFields', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->alert_table_fields_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AlertTableFields', 'url'=>array('admin')),
);
?>

<h1>View AlertTableFields #<?php echo $model->alert_table_fields_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'alert_table_fields_id',
		'alert_table_id',
		'alert_table_fields_name',
		'alert_field_frequency',
		'alert_table_fields_condition',
	),
)); ?>
