<?php
/* @var $this AlertTableFieldsController */
/* @var $model AlertTableFields */

$this->breadcrumbs=array(
	'Alert Table Fields'=>array('index'),
	$model->alert_table_fields_id=>array('view','id'=>$model->alert_table_fields_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AlertTableFields', 'url'=>array('index')),
	array('label'=>'Create AlertTableFields', 'url'=>array('create')),
	array('label'=>'View AlertTableFields', 'url'=>array('view', 'id'=>$model->alert_table_fields_id)),
	array('label'=>'Manage AlertTableFields', 'url'=>array('admin')),
);
?>

<h1>Update AlertTableFields <?php echo $model->alert_table_fields_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>