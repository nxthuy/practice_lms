<?php
/* @var $this AlertTableFieldsController */
/* @var $model AlertTableFields */

$this->breadcrumbs=array(
	'Alert Table Fields'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AlertTableFields', 'url'=>array('index')),
	array('label'=>'Create AlertTableFields', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#alert-table-fields-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Alert Table Fields</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alert-table-fields-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'alert_table_fields_id',
		'alert_table_id',
		'alert_table_fields_name',
		'alert_field_frequency',
		'alert_table_fields_condition',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
