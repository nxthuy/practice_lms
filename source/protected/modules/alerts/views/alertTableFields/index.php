<?php
/* @var $this AlertTableFieldsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Alert Table Fields',
);

$this->menu=array(
	array('label'=>'Create AlertTableFields', 'url'=>array('create')),
	array('label'=>'Manage AlertTableFields', 'url'=>array('admin')),
);
?>

<h1>Alert Table Fields</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
