<?php
/* @var $this AlertTableFieldsController */
/* @var $model AlertTableFields */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'alert_table_fields_id'); ?>
		<?php echo $form->textField($model,'alert_table_fields_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_table_id'); ?>
		<?php echo $form->textField($model,'alert_table_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_table_fields_name'); ?>
		<?php echo $form->textField($model,'alert_table_fields_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_field_frequency'); ?>
		<?php echo $form->textField($model,'alert_field_frequency'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_table_fields_condition'); ?>
		<?php echo $form->textField($model,'alert_table_fields_condition',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->