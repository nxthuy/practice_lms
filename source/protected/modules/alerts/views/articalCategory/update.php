<?php
/* @var $this ArticalCategoryController */
/* @var $model ArticalCategory */

$this->breadcrumbs=array(
	'Artical Categories'=>array('index'),
	$model->article_category_id=>array('view','id'=>$model->article_category_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ArticalCategory', 'url'=>array('index')),
	array('label'=>'Create ArticalCategory', 'url'=>array('create')),
	array('label'=>'View ArticalCategory', 'url'=>array('view', 'id'=>$model->article_category_id)),
	array('label'=>'Manage ArticalCategory', 'url'=>array('admin')),
);
?>

<h1>Update ArticalCategory <?php echo $model->article_category_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>