<?php
/* @var $this ArticalCategoryController */
/* @var $model ArticalCategory */

$this->breadcrumbs=array(
	'Artical Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ArticalCategory', 'url'=>array('index')),
	array('label'=>'Manage ArticalCategory', 'url'=>array('admin')),
);
?>

<h1>Create ArticalCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>