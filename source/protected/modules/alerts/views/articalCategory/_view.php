<?php
/* @var $this ArticalCategoryController */
/* @var $data ArticalCategory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('article_category_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->article_category_id), array('view', 'id'=>$data->article_category_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('article_category_name')); ?>:</b>
	<?php echo CHtml::encode($data->article_category_name); ?>
	<br />


</div>