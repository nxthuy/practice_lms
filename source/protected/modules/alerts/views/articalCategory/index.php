<?php
/* @var $this ArticalCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Artical Categories',
);

$this->menu=array(
	array('label'=>'Create ArticalCategory', 'url'=>array('create')),
	array('label'=>'Manage ArticalCategory', 'url'=>array('admin')),
);
?>

<h1>Artical Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
