<?php
/* @var $this AlertLogController */
/* @var $model AlertLog */

$this->breadcrumbs=array(
	'Alert Logs'=>array('index'),
	$model->alert_log_id,
);

$this->menu=array(
	array('label'=>'List AlertLog', 'url'=>array('index')),
	array('label'=>'Create AlertLog', 'url'=>array('create')),
	array('label'=>'Update AlertLog', 'url'=>array('update', 'id'=>$model->alert_log_id)),
	array('label'=>'Delete AlertLog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->alert_log_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AlertLog', 'url'=>array('admin')),
);
?>

<h1>View AlertLog #<?php echo $model->alert_log_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'alert_log_id',
		'alert_id',
		'content',
		'created_time',
	),
)); ?>
