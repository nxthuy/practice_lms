<?php
/* @var $this AlertLogController */
/* @var $model AlertLog */

$this->breadcrumbs=array(
	'Alert Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AlertLog', 'url'=>array('index')),
	array('label'=>'Manage AlertLog', 'url'=>array('admin')),
);
?>

<h1>Create AlertLog</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>