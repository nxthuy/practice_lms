<?php
/* @var $this AlertLogController */
/* @var $data AlertLog */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_log_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->alert_log_id), array('view', 'id'=>$data->alert_log_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_id')); ?>:</b>
	<?php echo CHtml::encode($data->alert_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />


</div>