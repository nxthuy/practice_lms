<?php
/* @var $this AlertLogController */
/* @var $model AlertLog */

$this->breadcrumbs=array(
	'Alert Logs'=>array('index'),
	$model->alert_log_id=>array('view','id'=>$model->alert_log_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AlertLog', 'url'=>array('index')),
	array('label'=>'Create AlertLog', 'url'=>array('create')),
	array('label'=>'View AlertLog', 'url'=>array('view', 'id'=>$model->alert_log_id)),
	array('label'=>'Manage AlertLog', 'url'=>array('admin')),
);
?>

<h1>Update AlertLog <?php echo $model->alert_log_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>