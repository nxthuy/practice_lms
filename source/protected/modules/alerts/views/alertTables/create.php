<?php
/* @var $this AlertTablesController */
/* @var $model AlertTables */

$this->breadcrumbs=array(
	'Alert Tables'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AlertTables', 'url'=>array('index')),
	array('label'=>'Manage AlertTables', 'url'=>array('admin')),
);
?>

<h1>Create AlertTables</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>