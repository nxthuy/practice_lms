<?php
/* @var $this AlertTablesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Alert Tables',
);

$this->menu=array(
	array('label'=>'Create AlertTables', 'url'=>array('create')),
	array('label'=>'Manage AlertTables', 'url'=>array('admin')),
);
?>

<h1>Alert Tables</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
