<?php
/* @var $this AlertTablesController */
/* @var $model AlertTables */

$this->breadcrumbs=array(
	'Alert Tables'=>array('index'),
	$model->alert_table_id=>array('view','id'=>$model->alert_table_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AlertTables', 'url'=>array('index')),
	array('label'=>'Create AlertTables', 'url'=>array('create')),
	array('label'=>'View AlertTables', 'url'=>array('view', 'id'=>$model->alert_table_id)),
	array('label'=>'Manage AlertTables', 'url'=>array('admin')),
);
?>

<h1>Update AlertTables <?php echo $model->alert_table_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>