<?php
/* @var $this AlertCategoryController */
/* @var $model AlertCategory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'alert-category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->textField($model,'category_id'); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_category_name'); ?>
		<?php echo $form->textField($model,'alert_category_name',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'alert_category_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_category_startus'); ?>
		<?php echo $form->textField($model,'alert_category_startus',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'alert_category_startus'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->