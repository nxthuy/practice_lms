<?php
/* @var $this AlertCategoryController */
/* @var $model AlertCategory */

$this->breadcrumbs=array(
	'Alert Categories'=>array('index'),
	$model->category_id=>array('view','id'=>$model->category_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AlertCategory', 'url'=>array('index')),
	array('label'=>'Create AlertCategory', 'url'=>array('create')),
	array('label'=>'View AlertCategory', 'url'=>array('view', 'id'=>$model->category_id)),
	array('label'=>'Manage AlertCategory', 'url'=>array('admin')),
);
?>

<h1>Update AlertCategory <?php echo $model->category_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>