<?php
/* @var $this AlertCategoryController */
/* @var $model AlertCategory */

$this->breadcrumbs=array(
	'Alert Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AlertCategory', 'url'=>array('index')),
	array('label'=>'Manage AlertCategory', 'url'=>array('admin')),
);
?>

<h1>Create AlertCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>