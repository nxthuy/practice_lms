<?php
/* @var $this AlertCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Alert Categories',
);

$this->menu=array(
	array('label'=>'Create AlertCategory', 'url'=>array('create')),
	array('label'=>'Manage AlertCategory', 'url'=>array('admin')),
);
?>

<h1>Alert Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
