<?php
/* @var $this AlertCategoryController */
/* @var $model AlertCategory */

$this->breadcrumbs=array(
	'Alert Categories'=>array('index'),
	$model->category_id,
);

$this->menu=array(
	array('label'=>'List AlertCategory', 'url'=>array('index')),
	array('label'=>'Create AlertCategory', 'url'=>array('create')),
	array('label'=>'Update AlertCategory', 'url'=>array('update', 'id'=>$model->category_id)),
	array('label'=>'Delete AlertCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->category_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AlertCategory', 'url'=>array('admin')),
);
?>

<h1>View AlertCategory #<?php echo $model->category_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'category_id',
		'alert_category_name',
		'alert_category_startus',
	),
)); ?>
