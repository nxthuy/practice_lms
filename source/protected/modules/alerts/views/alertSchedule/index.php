<?php
/* @var $this AlertScheduleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Alert Schedules',
);

$this->menu=array(
	array('label'=>'Create AlertSchedule', 'url'=>array('create')),
	array('label'=>'Manage AlertSchedule', 'url'=>array('admin')),
);
?>

<h1>Alert Schedules</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
