<?php
/* @var $this AlertScheduleController */
/* @var $data AlertSchedule */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_schedule_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->alert_schedule_id), array('view', 'id'=>$data->alert_schedule_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_schedule_name')); ?>:</b>
	<?php echo CHtml::encode($data->alert_schedule_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_schedule_command')); ?>:</b>
	<?php echo CHtml::encode($data->alert_schedule_command); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_schedule_status')); ?>:</b>
	<?php echo CHtml::encode($data->alert_schedule_status); ?>
	<br />


</div>