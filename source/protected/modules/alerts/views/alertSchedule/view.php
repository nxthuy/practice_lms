<?php
/* @var $this AlertScheduleController */
/* @var $model AlertSchedule */

$this->breadcrumbs=array(
	'Alert Schedules'=>array('index'),
	$model->alert_schedule_id,
);

$this->menu=array(
	array('label'=>'List AlertSchedule', 'url'=>array('index')),
	array('label'=>'Create AlertSchedule', 'url'=>array('create')),
	array('label'=>'Update AlertSchedule', 'url'=>array('update', 'id'=>$model->alert_schedule_id)),
	array('label'=>'Delete AlertSchedule', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->alert_schedule_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AlertSchedule', 'url'=>array('admin')),
);
?>

<h1>View AlertSchedule #<?php echo $model->alert_schedule_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'alert_schedule_id',
		'alert_schedule_name',
		'alert_schedule_command',
		'alert_schedule_status',
	),
)); ?>
