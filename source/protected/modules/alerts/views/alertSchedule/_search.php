<?php
/* @var $this AlertScheduleController */
/* @var $model AlertSchedule */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'alert_schedule_id'); ?>
		<?php echo $form->textField($model,'alert_schedule_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_schedule_name'); ?>
		<?php echo $form->textField($model,'alert_schedule_name',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_schedule_command'); ?>
		<?php echo $form->textField($model,'alert_schedule_command',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_schedule_status'); ?>
		<?php echo $form->textField($model,'alert_schedule_status',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->