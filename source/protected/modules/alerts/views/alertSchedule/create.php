<?php
/* @var $this AlertScheduleController */
/* @var $model AlertSchedule */

$this->breadcrumbs=array(
	'Alert Schedules'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AlertSchedule', 'url'=>array('index')),
	array('label'=>'Manage AlertSchedule', 'url'=>array('admin')),
);
?>

<h1>Create AlertSchedule</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>