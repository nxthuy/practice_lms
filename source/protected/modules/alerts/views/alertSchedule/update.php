<?php
/* @var $this AlertScheduleController */
/* @var $model AlertSchedule */

$this->breadcrumbs=array(
	'Alert Schedules'=>array('index'),
	$model->alert_schedule_id=>array('view','id'=>$model->alert_schedule_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AlertSchedule', 'url'=>array('index')),
	array('label'=>'Create AlertSchedule', 'url'=>array('create')),
	array('label'=>'View AlertSchedule', 'url'=>array('view', 'id'=>$model->alert_schedule_id)),
	array('label'=>'Manage AlertSchedule', 'url'=>array('admin')),
);
?>

<h1>Update AlertSchedule <?php echo $model->alert_schedule_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>