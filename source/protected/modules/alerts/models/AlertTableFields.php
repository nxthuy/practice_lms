<?php

/**
 * This is the model class for table "alert_table_fields".
 *
 * The followings are the available columns in table 'alert_table_fields':
 * @property integer $alert_table_fields_id
 * @property integer $alert_table_id
 * @property string $alert_table_fields_name
 * @property integer $alert_field_frequency
 * @property string $alert_table_fields_condition
 *
 * The followings are the available model relations:
 * @property AlertTables $alertTable
 */
class AlertTableFields extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alert_table_fields';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alert_table_id, alert_field_frequency', 'numerical', 'integerOnly'=>true),
			array('alert_table_fields_name, alert_table_fields_condition', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('alert_table_fields_id, alert_table_id, alert_table_fields_name, alert_field_frequency, alert_table_fields_condition', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'alertTable' => array(self::BELONGS_TO, 'AlertTables', 'alert_table_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'alert_table_fields_id' => 'Alert Table Fields',
			'alert_table_id' => 'Alert Table',
			'alert_table_fields_name' => 'Alert Table Fields Name',
			'alert_field_frequency' => 'Alert Field Frequency',
			'alert_table_fields_condition' => 'Alert Table Fields Condition',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('alert_table_fields_id',$this->alert_table_fields_id);
		$criteria->compare('alert_table_id',$this->alert_table_id);
		$criteria->compare('alert_table_fields_name',$this->alert_table_fields_name,true);
		$criteria->compare('alert_field_frequency',$this->alert_field_frequency);
		$criteria->compare('alert_table_fields_condition',$this->alert_table_fields_condition,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlertTableFields the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
