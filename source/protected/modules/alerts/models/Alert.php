<?php

/**
 * This is the model class for table "alert".
 *
 * The followings are the available columns in table 'alert':
 * @property integer $alert_id
 * @property integer $alert_category_id
 * @property integer $alert_schedule_id
 * @property integer $alert_table
 * @property string $alert_subject
 * @property string $alert_body
 * @property string $alert_type
 *
 * The followings are the available model relations:
 * @property AlertCategory $alertCategory
 * @property AlertSchedule $alertSchedule
 * @property AlertTables $alertTable
 * @property AlertLog[] $alertLogs
 */
class Alert extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alert';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alert_category_id, alert_schedule_id, alert_table', 'numerical', 'integerOnly'=>true),
			array('alert_subject', 'length', 'max'=>100),
			array('alert_type', 'length', 'max'=>12),
			array('alert_body', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('alert_id, alert_category_id, alert_schedule_id, alert_table, alert_subject, alert_body, alert_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'alertCategory' => array(self::BELONGS_TO, 'AlertCategory', 'alert_category_id'),
			'alertSchedule' => array(self::BELONGS_TO, 'AlertSchedule', 'alert_schedule_id'),
			'alertTable' => array(self::BELONGS_TO, 'AlertTables', 'alert_table'),
			'alertLogs' => array(self::HAS_MANY, 'AlertLog', 'alert_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'alert_id' => 'Alert',
			'alert_category_id' => 'Alert Category',
			'alert_schedule_id' => 'Alert Schedule',
			'alert_table' => 'Alert Table',
			'alert_subject' => 'Alert Subject',
			'alert_body' => 'Alert Body',
			'alert_type' => 'Alert Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('alert_id',$this->alert_id);
		$criteria->compare('alert_category_id',$this->alert_category_id);
		$criteria->compare('alert_schedule_id',$this->alert_schedule_id);
		$criteria->compare('alert_table',$this->alert_table);
		$criteria->compare('alert_subject',$this->alert_subject,true);
		$criteria->compare('alert_body',$this->alert_body,true);
		$criteria->compare('alert_type',$this->alert_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Alert the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
