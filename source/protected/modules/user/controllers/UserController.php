<?php

class UserController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout='//layouts/column2';

    public $defaultAction = 'login';

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),

            // For HOAuth
           'oauth' => array(
                // the list of additional properties of this action is below
                'class'=>'application.modules.user.extensions.hoauth.HOAuthAction',
                // Yii alias for your user's model, or simply class name, when it already on yii's import path
                // default value of this property is: User
                'model' => 'User',
                // map model attributes to attributes of user's social profile
                // model attribute => profile attribute
                // the list of avaible attributes is below

                /*'attributes' => array(
                    'email' => 'email',
                    'fname' => 'firstName',
                    'lname' => 'lastName',
                    'gender' => 'genderShort',
                    'birthday' => 'birthDate',
                    // you can also specify additional values,
                    // that will be applied to your model (eg. account activation status)
                    'acc_status' => 1,
                ),*/

                'attributes' => array(
                    'user_email' => 'email',
                    'user_status' => 1,
                ),
            )
            // this is an admin action that will help you to configure HybridAuth
            // (you must delete this action, when you'll be ready with configuration, or
            // specify rules for admin role. User shouldn't have access to this action!)
            /*'oauthadmin' => array(
                'class'=>'application.modules.user.extensions.hoauth.HOAuthAdminAction',
            ),*/
            //END for HOAuth
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('detail'),
                'users' => array('@'),
            ),
            array('allow', // allow all users to perform
                'actions' => array(
                    'captcha',
                    'login',
                    'googleLogin',
                    'oAuthLogin',
                    'oauth',
                    'logout',
                    'forgot',
                    'registration',
                    'activation',
                    'recovery',
                    'resetPassword',
                    'newPassword',
                    'profile',
                    'image',
                ),

                //'MyProfile'
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            )
        );
    }


    /* public function actionMyProfile() {
         $user = User::model()->findByPk(Yii::app()->user->getId());
         $profile = $user->profile;

         $this->render('profile',array(
             'model'=>$user, $profile)
         );
     }
 */

    public function actionProfile()
    {
        Yii::import('application.vendor.imageResize.*');
        require_once('ImageResize.php');

        //Yii::app()->theme='admin';
        $oldImage = '';

//        $user = User::model()->findByPk(Yii::app()->user->getId());
//        $userprofile = $user->profile;
        $userProfile = UserProfile::model()->with('user')->findByAttributes(array(
            'user_id' => Yii::app()->user->getId()
        ));


        if ($userProfile->user_photo != 'photo.png') {
            $oldImage = $userProfile->user_photo;
        }

        $userProfile->setScenario('profileUpdate');

        if (isset($_POST['UserProfile'])) {
            $userProfile->attributes = $_POST['UserProfile'];
            $userProfile->user_gender = $_POST['UserProfile']['user_gender'];
            $userSignatureFile = CUploadedFile::getInstance($userProfile, 'uploadedFile');
            $userImageUploadFile = CUploadedFile::getInstance($userProfile, 'userImage');

            if (!empty($userSignatureFile)) {
                $basePath = dirname(Yii::app()->request->scriptFile) . '/uploads/';
                $rnd = md5('BDUOIU*&*(=-0_)' . rand(0, 999999) . 'TNKKOU**(');
                $fileName = "{$rnd}.{$userSignatureFile->extensionName}";

                if ($userSignatureFile->saveAs($basePath . $fileName)) {
                    $userProfile->user_signature_name = $userSignatureFile->name;
                    $userProfile->user_signature_type = $userSignatureFile->type;

                    $imageResize = new ImageResize();
                    $imageResize->setBasePath($basePath);
                    $imageResize->load($fileName);
                    $imageResize->resize(UserProfile::SIGNATURE_IMAGE_HEIGHT, UserProfile::SIGNATURE_IMAGE_WIDTH);
                    $imageResize->setBasePath($basePath);
                    $imageResize->save($fileName);
                    $userProfile->user_signature_size = $imageResize->getImageSizeInByte();
                    $userProfile->user_signature = base64_encode(file_get_contents($basePath . $fileName));
                    if (file_exists($basePath . $fileName))
                        unlink($basePath . $fileName);
                }

            }
            if (!empty($userImageUploadFile)) {
                $rnd = rand(0, 9999);
                $fileName = "{$userProfile->user->user_name}-{$rnd}.{$userImageUploadFile->extensionName}";  // random number + file name
                $userProfile->user_photo = $fileName;

                $basePath = dirname(Yii::app()->request->scriptFile) . '/images/user/';

                Yii::app()->user->setState('userImage', $userProfile->user_photo);
                $userImageUploadFile->saveAs($basePath . $userProfile->user_photo);
                if (!empty($oldImage)) {
                    if (file_exists($basePath . $oldImage))
                        unlink($basePath . $oldImage);
                }
                $imageResize = new ImageResize();
                $imageResize->setBasePath($basePath);
                $imageResize->load($userProfile->user_photo);
                $imageResize->resize(UserProfile::IMAGE_HEIGHT, UserProfile::IMAGE_WIDTH);
                $imageResize->setBasePath($basePath);
                $imageResize->save($userProfile->user_photo);
                $userProfile->save(false);
            }

            if ($userProfile->save()) {
                Yii::app()->user->setFlash('success', 'Your Profile has been updated.');

            }
        }

        $this->render('_profile', array('model' => $userProfile));
    }

    public function actionImage()
    {
        //Yii::app()->theme = 'admin';

        $userProfile = UserProfile::model()->findByAttributes(array(
            'user_id' => Yii::app()->user->getId()
        ));

        //$userProfile->user_signature =  $userProfile->encrypt_decrypt('decrypt', $userProfile->user_signature, $userProfile->user_signature_salt);
        $this->render('image', array('model' => $userProfile));
    }

    /**
     * For use this function you have pass user model object
     * If Success return true otherwise you will get false
     */
    private function activationMail($model)
    {
        $activation_url = $this->createAbsoluteUrl('/user/activation', array("key" => $model->user_activekey, "email" => $model->user_email));
        $template = '<div style="text-align:left; width:600px; border:2px solid #4582dd; border-radus:3px; padding:5px">
                            <p>Hello ' . $model->user_name . ' ,</p>
                            <p>Thanks for registering with ' . Yii::app()->name . '</p>
                            <p>Please follow the link below to activate your account.</p>
                            <p>' . CHtml::link('Please click here to active your account.', $activation_url) . '</p>
                            <p>Regards,</p>
                            <p>WPSaePortal Team</p>
                            </div>';

        if (UserModule::sendMail($model->user_email, UserModule::t("WPSaePortal registration confirmation", array('{site_name}' => Yii::app()->name)), $template)) {
            return true;
        } else {
            return false;
        }
    }

    public function actionRegistration()
    {
        $model = new User();
        $model->setScenario('registration');
        $this->performAjaxValidation($model);


        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->user_status = 'Active';
            if (Yii::app()->controller->module->sendActivationMail)
                $model->user_status = 'Inactive';

            if ($model->save()) {
                $profile = new UserProfile();
                $profile->setScenario('registration');
                $profile->user_id = $model->user_id;
                $profile->user_dob = null;
                $profile->user_photo = 'photo.png';

                if ($profile->save()) {
                    if (Yii::app()->controller->module->sendActivationMail) {
                        $subscription = new SubscriptionHistory();
                        $subscription->setScenario('registration');
                        $subscription->user_id = $model->user_id;
                        $subscription->product_category_id = 5; // basic product category id is 5
                        $subscription->approved_by = 2; // user system id is 2
                        $subscription->approved_time = time();
                        $subscription->request_time = time();

                        if ($subscription->save()) {

                            $user = User::model()->findByPk($model->user_id);
                            $user->setScenario('addEmployerUser');
                            $user->subscription_history_id = $subscription->subscription_history_id;
                            if ($user->save()) {
                                if ($this->activationMail($user))
                                    Yii::app()->user->setFlash('success', 'Thank you for signup. Please check your mail to active your account.');
                                else {
                                    Yii::app()->user->setFlash('success', 'Thank you for signup. Please check your mail to active your account.');
                                }
                            }
                        }
                    } else {
                        Yii::app()->user->setFlash('success', 'Thank you for signup.');
                    }
                    $this->redirect(array('login'));
                }
            }
        }

        $this->render('registration', array(
            'model' => $model,
        ));
    }

    public function actionActivation()
    {
        $key = Yii::app()->getRequest()->getParam('key');
        $email = Yii::app()->getRequest()->getParam('email');

        if (isset($key) && isset($email)) {
            $user = User::model()->findByAttributes(array(
                'user_activekey' => $key,
                'user_email' => $email,
                'user_status' => 'Inactive'
            ));

            if (!isset($user->user_id)) {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
            $user->setScenario('active');
            $user->user_status = 'Active';
            if ($user->save()) {
                Yii::app()->user->setFlash('activationSuccess', 'Congratulation your account has been activated.');
            } else {
                Yii::app()->user->setFlash('activationError', 'Congratulation your account has been activated.');
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }

        $this->render('activation');
    }

    private function passwordResetMail($model)
    {
        $activation_url = $this->createAbsoluteUrl('/user/ResetPassword', array("key" => $model->user_activekey, "email" => $model->user_email));
        $template = '<div style="text-align:left; width:600px; border:2px solid #4582dd; border-radus:3px; padding:5px">
                            <p>Hello ' . $model->user_name . ' ,</p>
                            <p>We have received a request to reset your password.</p>
                            <p>Please confirm by clicking ' . CHtml::link('here', $activation_url) . ' to reset your password.</p>
                            <p>Subsequently, we will send you a temproray password for you to login and then you can change your password after the login.</p>
                            <p>Regards,</p>
                            <p>WPSaePortal Team</p>
                            </div>';

        if (UserModule::sendMail($model->user_email, UserModule::t("WPSaePortal registration confirmation", array('{site_name}' => Yii::app()->name)), $template)) {
            return true;
        } else {
            return false;
        }
    }
    /*
    public function actionRecovery()
    {
        $user = new User();
        $user->setScenario('recovery');

        if (Yii::app()->user->getState('attempts-login') > 2) {
            $user->setScenario('withCaptcha');
        }

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if ($user->validate()) {
                $model = User::model()->findByPk($user->user_id);
                $model->setScenario('recoverActiveKey');
                $model->user_activekey = uniqid(rand(999, 9999));

                if ($model->save()) {
                    Yii::app()->user->setState('attempts-login', 0);
                    if ($this->passwordResetMail($model)) {
                        Yii::app()->user->setFlash('success', 'Congratulation please check your mail to active your account.');
                        $this->redirect(array('user/login'));
                    }
                }
            }
        }

        $this->render('recovery', array(
            'model' => $user
        ));
    }
    */

    private function passwordSendMail($model, $password)
    {
        $activation_url = $this->createAbsoluteUrl('/user/login');


        $template = '<div style="text-align:left; width:600px; border:2px solid #4582dd; border-radus:3px; padding:5px">
                            <p>Hello ' . $model->user_name . ' ,</p>
                            <p>Your new temporary password is: <b>' . $password . '</b> ,</p>
                            <p>This password will expiry on :  ' . date('Y/m/d h:i:s A', $model->user_password_expiry_time) . ' ,</p>
                            <p>Please follow the link below to activate your account.</p>
                            <p>' . CHtml::link('Please click here to active your account.', $activation_url) . '</p>
                            <p>Regards,</p>
                            <p>WPSaePortal Team</p>
                            </div>';

        if (UserModule::sendMail($model->user_email, UserModule::t("WPSaePortal registration confirmation", array('{site_name}' => Yii::app()->name)), $template)) {
            return true;
        } else {
            return false;
        }
    }

    public function actionResetPassword()
    {
        $key = Yii::app()->getRequest()->getParam('key');
        $email = Yii::app()->getRequest()->getParam('email');

        if (isset($key) && isset($email)) {
            $user = User::model()->findByAttributes(array(
                'user_activekey' => $key,
                'user_email' => $email,
            ));

            if (!isset($user->user_id)) {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
            $user->setScenario('active');
            $user->user_activekey = md5(uniqid(rand(999, 9999)));//Used to automaticaly generate password.
            $user->user_status = 'Active';
            $password = uniqid(rand(999, 99999));
            $user->user_password = UserModule::encrypting($password);//mã hóa dữ liệu
            $user->user_password_expiry_time = time() + (6 * 60 * 60);
            if ($user->save()) {
                if ($this->passwordSendMail($user, $password)) {
                    Yii::app()->user->setFlash('success', 'Thank you. Please check your mail to we sent new temporary password.');
                    $this->redirect(array('user/login'));
                }
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionNewPassword()
    {
        $this->layout = "//layouts/login";
        //Yii::app()->theme='admin';

        $model = User::model()->findByPk(Yii::app()->user->getId());
        $model->setScenario('changePassword');//Sets the scenario for the model.

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'New Password has been updated.');
                $this->refresh();
            }

        }
        $this->render('newPassword', array(
            'model' => $model,
        ));
    }

    public function actionLogin()
    {

        $this->layout = "//layouts/login";

        $model = new LoginForm();
        $modelForgot = new ForgotForm();
        /*if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }*/

        if (isset($_GET['code'])) {
            dump(Hybrid_Auth::getConnectedProviders());
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (!empty(Yii::app()->user->returnUrl))
                    $this->redirect(Yii::app()->user->returnUrl);
                else
                    $this->redirect(array('site/index'));
            }
        }

        $this->render('login', array(
            'model' => $model,
            'modelForgot' => $modelForgot
        ));
    }

    public function actionGoogleLogin()
    {
        //Yii::import('application.extensions.eoauth.*');

        $ui = new EOAuthUserIdentity(array(
            //Set the "scope" to the service you want to use
            'scope' => 'https://sandbox.google.com/apis/ads/publisher/',
            'provider' => array(
                'request' =>' https://www.google.com/accounts/OAuthGetRequestToken',
                'authorize' => 'https://www.google.com/accounts/OAuthAuthorizeToken',
                'access' => 'https://www.google.com/accounts/OAuthGetAccessToken',
            )
        ));

        if ($ui->authenticate()) {
            if (user()->login($ui)) {
                if (!empty(user()->returnUrl)) {
                    $this->redirect(user()->returnUrl);
                }
                else {
                    $this->redirect(array('site/index'));
                }
            }
        } else {
            throw new CHttpException(401, $ui->error);
        }
    }

    public function actionForgot()
    {
        $model = new ForgotForm();
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] == 'forgot-form') {
            $model->attributes = $_POST['ForgotForm'];
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        // collect user input data
            if (isset($_POST['ForgotForm'])) {
                $model->attributes = $_POST['ForgotForm'];
                if ($model->validate()) {
                    $userModel = User::model()->findByAttributes(array('user_email' => $model->email));
                    $key = $userModel->user_forgot_password_key = md5($userModel->user_id.time());
                    $userModel->save(false);

                    $params = array(
                        'data' => array(
                            'URL_RECOVERY' => url('user/recovery', array('key' => $key)),
                        ),
                        'to' => array('demo', $model->email),
                    );
                    CeresMail::queue('forgotPass', $params, EmailQueue::TYPE_URGENT);

                    Yii::app()->user->setFlash('warning', "Your request was successfully sent.<br /> Please check your email to change password.");
                    $this->redirect(array('user/login'));
                }
        }
        $this->render('_forgot', array(
            'model' => $model,
        ));
    }

    public function actionRecovery()
    {
        $key = $_GET['key'];
        $userModel = User::model()->findByAttributes(array('user_forgot_password_key' => $key));
        if ($userModel==NULL) {
            Yii::app()->user->setFlash('danger','Invalid forgot password code.');
            $this->redirect(array('user/login'));
        } else {
            $this->layout = "//layouts/login";
            $model = new RecoveryForm();

            if (isset($_POST['RecoveryForm'])) {
                $model->attributes = $_POST['RecoveryForm'];
                if ($model->validate()) {
                    $userModel->user_password = md5($model->password);
                    $userModel->user_forgot_password_key = NULL;
                    $userModel->save(false);

                    Yii::app()->user->setFlash('success','Your password was successfully changed.');
                    $this->redirect(array('user/login'));
                }
            }
            $this->render('recovery', array(
                'model' => $model,
            ));
        }
    }


    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);//It seems like you can configure it in your main.php like this
    }

    public  function actionDetail($id)
    {
        $user = User::model()->findByPk($id);
        $profile = $user->profile;

        $this->render('profileMember', array('model' => $user, $profile));
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */


}
