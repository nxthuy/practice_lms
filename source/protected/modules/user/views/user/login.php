<?php
/* @var $this SiteController */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>

<head>
    <title>Login</title>
</head>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnChange' => false,
    ),
    'htmlOptions' => array('class' => 'form-horizontal clearfix')
));

?>

<h1>Login</h1>

<div class="form-group">
    <div class="input-group col-xs-12">
        <?php
        echo $form->textField($model, 'username', array(
            'class' => 'form-control',
            'placeholder' => 'Username'
        ));
        ?>
        <?php echo $form->error($model, 'username') ?>
    </div>
</div>
<div class="form-group">
    <div class="input-group col-xs-12">
        <?php
        echo $form->passwordField($model, 'password', array(
            'class' => 'form-control',
            'placeholder' => 'Password'
        ));
        ?>
        <?php echo $form->error($model, 'password') ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-8">
        <div class="btn-group but-checkbox" data-toggle="buttons">
            <label class="btn btn-checkbox">
                <input type="checkbox">
            </label>
            <label>Remember me</label>
        </div>
    </div>
    <div class="col-xs-3 pull-right">
        <?php
        echo CHtml::submitButton(UserModule::t("Login"), array(
            'class' => 'btn btn btn-success'
        ));
        ?>
    </div>
</div>
<?php $this->widget('application.modules.user.extensions.hoauth.widgets.HOAuth'); ?>
<div class="forgot">
    <a href="#view" data-toggle="modal" class="view" title="">
        Forgot password?
    </a>
</div>

<?php
$this->endWidget();
$this->renderPartial('_forgot', array(
    'model' => $model,
    'modelForgot' => $modelForgot
));
?>
