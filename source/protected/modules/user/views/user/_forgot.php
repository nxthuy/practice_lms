<?php
$forgotForm = $this->beginWidget('CActiveForm', array(
    'id' => 'forgot-form',
    'action' => 'user/forgot',
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('class' => 'form-horizontal clearfix')
));
?>
    <!-- Modal View-->
    <div class="modal fade" id="view">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="pull-left">Forgot password</h2>

                    <div class="pull-right">
                        <a class="circle" href="#" aria-hidden="true" data-dismiss="modal" type="button">
                            <span class="glyphicon glyphicon-remove"></span>
                            <span class="text">Close</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 modal-body clearfix">
                    <form class="form-horizontal" role="form">
                        <h2>
                            <small>Input your email</small>
                        </h2>
                        <div class="hidden alert alert-success">An email has been sent to you with the information to
                            restore password.
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <!-- <input type="text" class="form-control" placeholder="Your Email"> -->
                                <?php
                                echo $forgotForm->textField($modelForgot, 'email', array(
                                    'class' => 'form-control',
                                    'placeholder' => 'Email'
                                ));
                                ?>
                                <?php echo $forgotForm->error($modelForgot, 'email') ?>
                            </div>

                        </div>
                        <div class="form-group col-xs-12">
                            <!-- <button type="submit" class="btn btn-success">Submit</button>
                            <button type="submit" class="btn btn-default">Cancel</button> -->
                            <?php
                            echo CHtml::submitButton(UserModule::t("Submit"), array(
                                'class' => 'btn btn btn-success'
                            ));
                            ?>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $this->endWidget(); ?>
