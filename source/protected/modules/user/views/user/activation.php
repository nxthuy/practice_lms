<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
/* hasFlash hien thi tinh static*/
?>



    <?php if(Yii::app()->user->hasFlash('activationSuccess')): ?>
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close"></button>
            <?php echo Yii::app()->user->getFlash('activationSuccess'); ?>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->hasFlash('activationError')): ?>
        <div class="alert alert-error">
            <button data-dismiss="alert" class="close"></button>
            <?php echo Yii::app()->user->getFlash('activationError'); ?>
        </div>
    <?php endif; ?>

    <div class="forget-actions">
    <p>
        <?php echo CHtml::link(UserModule::t("Click"),array('user/login')); ?>
        here to login your account.
    </p>
    </div>

