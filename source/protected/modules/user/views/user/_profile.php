<?php
/* @var $this UserProfileController */
/* @var $model UserProfile */
/* @var $model User */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Profile';
$this->breadcrumbs = array(
    'Profile',
);
?>
<head>
    <title>Update Profile</title>
</head>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => false,
    ),
    'htmlOptions' => array(
        'class' => 'form-horizontal clearfix',
        'enctype' => 'multipart/form-data'
    )));
?>

<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>




<section class="col-lg-12">

<h1 class="title">Update Profile's <?php echo $model->user->user_name; ?></h1>

<form class="form-horizontal col-lg-6" role="form">
<div class="form-group">
    <?php
    echo $form->labelEx($model, 'user_first_name', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-3">
        <?php
        echo $form->textField($model, 'user_first_name', array(
            'class' => 'form-control',
            'placeholder' => 'First Name',
            //'style' => 'height:35px; width:307px',
        ));
        ?>

        <?php echo $form->error($model, 'user_first_name'); ?>
    </div>
</div>


<div class="form-group">
    <?php
    echo $form->labelEx($model, 'user_last_name', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-3">
        <?php
        echo $form->textField($model, 'user_last_name', array(
            'class' => 'form-control',
            'placeholder' => 'Last Name',
            'style' => 'height:35px; width:307px',
        ));
        ?>

        <?php echo $form->error($model, 'user_last_name'); ?>
    </div>
</div>

<div class="form-group">
    <?php
    echo $form->labelEx($model, 'user_gender', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-3">
        <?php
        echo $form->radioButton($model, 'user_gender', array(
            'value' => "Male",
            'uncheckValue' => null,
            'style' => "margin-right: 10px;",
        ));
        echo "Male";

        echo $form->radioButton($model, 'user_gender', array(
            'value' => "Female",
            'uncheckValue' => null,
            'style' => "margin-right: 10px; margin-left: 10px;",
        ));
        echo "Female";

        echo $form->radioButton($model, 'user_gender', array(
            'value' => "Unknown",
            'uncheckValue' => null,
            'style' => "margin-right: 10px; margin-left: 10px;",
        ));
        echo "Other";
        ?>

        <?php echo $form->error($model, 'user_gender'); ?>
    </div>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'user_dob', array('class' => 'col-lg-2 control-label')); ?>
    <div class="col-lg-3">
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'user_dob',
            'model' => $model,
            'attribute' => 'user_dob',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => 'dd-mm-yy',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => '1920:' . (date('Y') - 18),
            ),
            'htmlOptions' => array(
                'class' => 'form-control',
                'style' => 'height:35px; width:307px',
                'value' => date('d/m/Y', strtotime($model->user_birthday)),
            ),
        ));
        ?>
        <?php echo $form->error($model, 'user_dob'); ?>
    </div>
</div>

<!--        <div class="form-group">
            //<?php
//            echo $form->labelEx($model, 'user_email', array(
//                'class' => 'col-lg-2 control-label'
//            ));
//            ?>
            <div class="col-lg-3">
                //<?php
//                echo $form->textField($model, 'user_email', array(
//                    'class' => 'form-control',
//                    'placeholder' => 'Email',
//                    'style' => 'height:35px; width:307px',
//                ));
//                ?>

                //<?php //echo $form->error($model, 'user_email'); ?>
            </div>
        </div>-->

<div class="form-group">
    <?php
    echo $form->labelEx($model, 'user_phone', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-3">
        <?php
        echo $form->textField($model, 'user_phone', array(
            'class' => 'form-control',
            'placeholder' => 'Phone Number',
            'style' => 'height:35px; width:307px',
        ));
        ?>

        <?php echo $form->error($model, 'user_phone'); ?>
    </div>
</div>

<div class="form-group">
    <?php
    echo $form->labelEx($model, 'user_address', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-3">
        <?php
        echo $form->textField($model, 'user_address', array(
            'class' => 'form-control',
            'placeholder' => 'Address',
            'style' => 'height:35px; width:307px',
        ));
        ?>

        <?php echo $form->error($model, 'user_address'); ?>
    </div>
</div>

<div class="form-group">
    <?php
    echo $form->labelEx($model, 'currentPhoto', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-3">
        <?php
        $path = Yii::app()->request->baseUrl . '/images/';
        (!empty($model->user_photo)) ? $path .= 'user/'. $model->user_photo : $path .= 'no_avatar.jpg';
        echo CHtml::image( $path, $model->user_photo, array(
            "width" => "100px", "height" => "100px"
        ));
        ?>
    </div>
</div>

<div class="form-group">
    <?php
    echo $form->labelEx($model, 'change_photo', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-3">
        <?php
        echo $form->fileField($model, 'userImage', array(
            'class' => 'form-control',
            'placeholder' => 'Signature',
            'style' => 'border-width: 0px;'
        ));
        ?>
    </div>
</div>

<?php if (Yii::app()->user->checkAccess('employer') || Yii::app()->user->checkAccess('approver')): ?>
    <div class="form-group">
        <?php
        echo $form->labelEx($model, 'user_signature', array(
            'class' => 'col-lg-2 control-label'
        ));
        ?>
        <div class="col-lg-3">
            <?php
            echo $form->fileField($model, 'uploadedFile', array(
                'class' => 'form-control',
                'placeholder' => 'Signature'
            ));
            ?>
            <?php echo $form->error($model, 'user_signature'); ?>
        </div>
    </div>
<?php endif; ?>

<div class="form-group">
    <?php
    echo $form->labelEx($model, '', array(
        'class' => 'col-lg-2 control-label'
    ));
    ?>
    <div class="col-lg-6">
        <?php echo CHtml::submitButton('Update', array('class' => 'btn-success')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn-default')); ?>
    </div>
</div>

<?php $this->endWidget(); ?>
</form>
</section>            
