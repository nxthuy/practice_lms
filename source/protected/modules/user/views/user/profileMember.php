<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<head>
    <title>User's Information</title>
</head>

<section class="col-xs-12">
    <h1 class="title">Profile's <?php echo $model->user_name; ?> </h1>
    <div class="row clearfix">
        <div class="col-xs-6 row">
            <div class="row-small">
                <div class="col-xs-4">
                    <img class="avatar" alt="" src="<?php echo (!empty($model->userProfiles->user_photo) ? '/images/user/' . $model->userProfiles->user_photo : '/images/no_avatar.jpg'); ?>">
                </div>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">First name:</span>
                <span class="col-xs-8"><?php echo $model->userProfiles['user_first_name']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">Last name:</span>
                <span class="col-xs-8"><?php echo $model->userProfiles['user_last_name']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">Gender:</span>
                <span class="col-xs-8"><?php echo $model->userProfiles['user_gender']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">Birthday:</span>
                <span class="col-xs-8"><?php echo date('d/m/Y', strtotime($model->userProfiles['user_dob'])); ?> </span>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">Email:</span>
                <span class="col-xs-8"><?php echo $model->user_email; ?></span>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">Phone:</span>
                <span class="col-xs-8"><?php echo $model->userProfiles['user_phone']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">Address:</span>
                <span class="col-xs-8"><?php echo $model->userProfiles['user_address']; ?></span>
            </div>
            <div class="row-small">
                <span class="col-xs-4 field-name">Position:</span>
                <span class="col-xs-8"><?php echo $model->userRole->user_role_name; ?></span>
            </div>
        </div>
    </div>
</section>