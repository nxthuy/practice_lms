<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<?php
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Registration");
$this->breadcrumbs = array(
    UserModule::t("Registration"),
);
?>

<?php if (Yii::app()->user->hasFlash('registration')): ?>
    <div class="success">
        <?php echo Yii::app()->user->getFlash('registration'); ?>
    </div>
<?php endif; ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('class' => 'form-vertical login-form'),
        ));
?>

<h3 class="form-title"><?php echo UserModule::t("Registration"); ?></h3>
<?php if ($model->hasErrors()): ?>
    <div class="alert alert-error hide" style="display: block;">
        <button class="close" data-dismiss="alert"></button>
    <?php echo CHtml::errorSummary($model); ?>

    </div>
    <?php endif; ?>

<div class="control-group">
<?php echo $form->labelEx($model, 'user_name', array('class' => 'control-label visible-ie8 visible-ie9')); ?>
    <div class="controls">
        <div class="input-icon left">
            <i class="icon-user"></i>
<?php
//labelEx: Renders an HTML label for a model attribute.
echo $form->textField($model, 'user_name', array(
    'class' => 'm-wrap placeholder-no-fix',
    'placeholder' => 'Username',
    'maxlength' => 50
));
?>
        </div>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'user_password', array('class' => 'control-label visible-ie8 visible-ie9')); ?>
    <div class="controls">
        <div class="input-icon left">
            <i class="icon-lock"></i>
            <?php
            echo $form->passwordField($model, 'user_password', array(
                'class' => 'm-wrap placeholder-no-fix',
                'placeholder' => 'Password',
                'maxlength' => 16
            ));
            ?>
        </div>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'verifyPassword', array('class' => 'control-label visible-ie8 visible-ie9')); ?>

    <div class="controls">
        <div class="input-icon left">
            <i class="icon-lock"></i>
            <?php
            echo $form->passwordField($model, 'verifyPassword', array(
                'class' => 'm-wrap placeholder-no-fix',
                'placeholder' => 'Verify Password',
                'maxlength' => 16
            ));
            ?>
        </div>
    </div>
</div>

<div class="control-group">
<?php echo $form->labelEx($model, 'user_email', array('class' => 'control-label visible-ie8 visible-ie9')); ?>
    <div class="controls">
        <div class="input-icon left">
            <i class="icon-envelope"></i>
            <?php
            echo $form->textField($model, 'user_email', array(
                'class' => 'm-wrap placeholder-no-fix',
                'placeholder' => 'Email',
                'maxlength' => 50
            ));
            ?>
        </div>
    </div>
</div>

<div>
    <p>
        <?php echo CHtml::link("Terms and Conditions", array('/terms-conditions')); ?>
    </p>
    <p>
<?php echo CHtml::link("Privacy Policy", array('/privacy-policy')); ?>
    </p>
</div>

<div class="control-group">
    <div class="controls">
        <div class="help-block">
<?php echo $form->checkBox($model, 'accept') ?> I Agree the terms and conditions.</div>
    </div>
</div>

<div class="form-actions">
    <?php
    echo CHtml::submitButton(UserModule::t("Register"), array(
        'class' => 'btn green pull-right'
    ));
    ?>
</div>

<?php $this->endWidget(); ?>