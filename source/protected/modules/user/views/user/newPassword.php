<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - NewPassword';
$this->breadcrumbs = array(
    'NewPassword',
);
?>
<head>
    <title>Change Password</title>>
</head>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => false,
        ),
        'htmlOptions' => array('class' => 'form-horizontal clearfix')
    ));
    ?>

    <h1><strong>Change Password</strong></h1>

    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close"></button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>

    <?php endif; ?>


    <div class="form-group">

        <div class="input-group col-xs-12">
            <?php
            echo $form->passwordField($model, 'oldPassword', array(
                'class' => 'form-control',
                'placeholder' => 'Old Password'
            ));
            ?>
            <?php echo $form->error($model, 'oldPassword'); ?>
        </div>
    </div>


    <div class="form-group">

        <div class="input-group col-xs-12">
            <?php
            echo $form->passwordField($model, 'newPassword', array(
                'class' => 'form-control',
                'placeholder' => 'New Password'
            ));
            ?>
            <?php echo $form->error($model, 'newPassword'); ?>
        </div>
    </div>

    <div class="form-group">

        <div class="input-group col-xs-12">
            <?php
            echo $form->passwordField($model, 'verifyPassword', array(
                'class' => 'form-control',
                'placeholder' => 'Verify Password'
            ));
            ?>
            <?php echo $form->error($model, 'verifyPassword'); ?>
        </div>
    </div>


    <div class="form-actions">
        <div class="col-lg-offset-4 col-lg-6">
            <?php echo CHtml::submitButton('Update', array('class' => 'btn-success')); ?>
            <?php echo CHtml::link('Cancel', "/", array('class' => 'btn-default')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>        
</div>



