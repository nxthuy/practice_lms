<?php
/* @var $this SiteController */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Recovery Password';
$this->breadcrumbs = array(
    'Recovery Password',
);
?>

<head>
    <title>Recovery Password</title>
</head>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'recovery-form',
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => false,
    ),
    'htmlOptions' => array('class' => 'form-horizontal clearfix')
));

?>

<h1>Recovery Password</h1>

<div class="form-group">
    <div class="input-group col-xs-12">
        <?php
        echo $form->passwordField($model, 'password', array(
            'class' => 'form-control',
            'placeholder' => 'New Password'
        ));
        ?>
        <?php echo $form->error($model, 'password') ?>
    </div>
</div>
<div class="form-group">
    <div class="input-group col-xs-12">
        <?php
        echo $form->passwordField($model, 'repassword', array(
            'class' => 'form-control',
            'placeholder' => 'Re New Password'
        ));
        ?>
        <?php echo $form->error($model, 'repassword') ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-3 pull-right">
        <?php
        echo CHtml::submitButton(UserModule::t("Change"), array(
            'class' => 'btn btn btn-success'
        ));
        ?>
    </div>
</div>

<?php
$this->endWidget();
?>
