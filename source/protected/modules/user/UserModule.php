<?php

class UserModule extends CWebModule
{
    public $defaultController='user';
    /**
     * @var boolean
     * @desc use email for activation user account
     */

    public $hash = 'sha1';

    public $sendActivationMail=true;

    public $registrationUrl = array("/user/registration");
    public $recoveryUrl = array("/user/recovery");
    public $loginUrl = array("/user/login");
    public $logoutUrl = array("/user/logout");
    public $profileUrl = array("/user/profile");
    public $returnUrl = array("/site/company");
    public $returnLogoutUrl = array("/user/login");

    /**
     * @var int
     * @desc Remember Me Time (seconds), defalt = 2592000 (30 days)
     */
    public $rememberMeTime = 2592000; // 30 days
    /**
     * @var boolean
     */
    //public $cacheEnable = false;

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		// import the module-level models and components
		$this->setImport(array(
            'user.models.*',
            'user.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

    public static function sendMail($email,$subject,$message) {
        $adminEmail = Yii::app()->params['adminEmail'];
        //$headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
        $message = wordwrap($message, 70);
        $message = str_replace("\n.", "\n..", $message);
        //return mail($email,'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);

        $from = Yii::app()->params['adminEmail'];
        $name = Yii::app()->name;

        $mail = new YiiMailer();
        $mail->setFrom($from, $name);
        $mail->setTo($email);
        $mail->setSubject($subject);
        $mail->setBody($message);

        $mail->IsSMTP();
        $mail->Host = Yii::app()->params['smtpHost'];
        $mail->Port = Yii::app()->params['smtpPort'];
        $mail->SMTPAuth = Yii::app()->params['smtpAuth'];
        $mail->Username = Yii::app()->params['smtpUser'];
        $mail->Password = Yii::app()->params['smtpPassword'];

        if ($mail->send()) {
            return true;
        } else {

            return false;
        }
    }

    public static function t($str='',$params=array(),$dic='user') {
        if (Yii::t("UserModule", $str)==$str)
            return Yii::t("UserModule.".$dic, $str, $params);
        else
            return Yii::t("UserModule", $str, $params);
    }

    public static function encrypting($string="")
    {
        $hasSalt = '$2a$10$1qAz2wSx3eDc4rFv5tGb5e4jVuld5/KF2Kpy.B8D2XoC031sReFGi';

        $hash = Yii::app()->getModule('user')->hash;
        if ($hash=="md5")
            return md5($string);
        if ($hash=="sha1")
            return hash('sha256', $hasSalt.$string);
        else
            return hash($hash,$string);
    }

}
