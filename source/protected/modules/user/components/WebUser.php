<?php
/**
 * Created by PhpStorm.
 * User: sajib
 * Date: 12/13/13
 * Time: 8:43 PM
 */

class WebUser extends CWebUser
{
    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array())
    {
        if (empty($this->id)) { // Not identified => no rights
            return false;
        }

        $role = trim($this->getState("roles"));  // Get role of user

        if ($role == $operation) {
            return true; // if operation is single role
        }

        if(in_array($role, array_map('trim',explode(',',$operation)))) {
            return true; //Check if multiple roles are available
        }

        return ($operation === $role);// allow access if the operation request is the current user's role
    }

    public function checkPackageAccess($packages)
    {
        if (empty($this->id)) { // Not identified => no rights
            return false;
        }

        $package = trim($this->getState("package"));  // Get role of user

        if ($package == $packages) {
            return true; // if operation is single role
        }

        if(in_array($package, array_map('trim',explode(',',$packages)))) {
            return true; //Check if multiple roles are available
        }

        return ($package === $packages);// allow access if the operation request is the current user's role
    }

    public function checkProPermission($packages, $task)
    {
        if (empty($this->id)) { // Not identified => no rights
            return false;
        }

        $package = trim($this->getState("package"));  // Get role of user
        $proPermission = Yii::app()->user->getState('pro');

        if ($package == $packages) {
            if(count($proPermission[$task])>0)
                return true;
        }

        if(in_array($package, array_map('trim',explode(',',$packages)))) {

            if(count($proPermission[$task])>0)
                return true;
        }

        return false;
    }


    public function checkDebitInstructionAccess($status)
    {

        $role = $this->getState("roles");

        if($role=='initiator' && $status==Sif::AUTHORIZATION_PENDING)
        {
            return false;
        }
        elseif($role=='employer' || $role=='employer admin')
        {
            return true;
        }
        else
            return true;

    }

}