<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
    const ERROR_EMAIL_INVALID = 3;
    const ERROR_NOT_ACTIVATED = 4;
    private $_id;

	public function authenticate()
	{

        array('allow', // allow authenticated user to perform 'create' and 'update' actions
                                'actions'=>array('create','update', 'newpassword'),
                                'users'=>array('@'));
                
        
        if (strpos($this->username,"@"))//username is email
        {
            $user = User::model()->findByAttributes(array(
                'user_email'=>$this->username,
                'user_password'=>UserModule::encrypting($this->password)
            ));


        }
        else // user is only string
        {
            $user = User::model()->findByAttributes(array(
                'user_name'=>$this->username,
                'user_password'=>UserModule::encrypting($this->password)
            ));
        }
        //print_r(UserModule::encrypting($this->password));die();
        if($user===null)
        {
            if (strpos($this->username,"@")) {
                $this->errorCode=self::ERROR_EMAIL_INVALID;
            } else {
                $this->errorCode=self::ERROR_USERNAME_INVALID;
            }
        }
        elseif($user->user_status=='Inactive')
        {
            $this->errorCode=self::ERROR_NOT_ACTIVATED;
        }
        else
        {
            $this->_id = $user->user_id;

            $data = array();
            $role = $user->userRole->user_role_name;

            if($role=='employer')
            {
                foreach($user->employers as $employer)
                {
                    $data[] = $employer->employer_id;
                }
            }
            elseif($role=='employer admin' || $role=='approver' || $role=='initiator')
            {
                foreach($user->employerUsers as $employer)
                {
                    $data[] = $employer->employer_id;
                }
            }

            $this->setState('employerId',$data);
            $this->setState('userImage', $user->userProfiles->user_photo);
            $this->setState('status', $user->user_status);
            $this->username = $user->user_name;
            $this->setState('roles',$role);

            $role_id = $user->user_role_id;
            switch ($role_id){
                case 1:// admin
                    $this->setState('role',User::ROLE_ADMIN);
                    break;
                case 2:// manager
                    $this->setState('role',User::ROLE_MANAGER);
                    break;
                case 3:// admin
                    $this->setState('role',User::ROLE_GENERAL);
                    break;
                default:
                    break;

            }


            $this->setState('email',$user->user_email);

            if($role=='employer')
            {
                $this->setState('package',$user->subscription->productCategory->product_category_name);

                $pro = array(
                    'payroll'=>array(),
                    'employer document'=>array(),
                    'employee document'=>array()
                );

                $model = SponsorAndProRequest::model()->findByAttributes(array(
                    'user_id'=>$user->user_id,
                    'type'=>'PRO'
                ));

                if(isset($model))
                {
                    foreach($model->proPermission as $permission)
                    {
                        $pro[$permission->task][] = $permission->action;
                    }
                }

                $this->setState('pro', $pro);
            }

            $this->errorCode=self::ERROR_NONE;
        }

         return $this->errorCode;
	}

    public function getId()
    {
        return $this->_id;
    }

}