<?php

class ForgotForm extends CFormModel
{
    public $email;
    //private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that mail needs to be required and exist
     */
    public function rules()
    {
        return array(
            // email is required
            array('email', 'required'),
            // email need exist
            array('email', 'emailexist'),
        );
    }

    public function emailexist($attribute, $params)
    {
        $userModel = User::model()->findByAttributes(array('user_email' => $this->email));
        if ($userModel == NULL) {
            $this->addError('email', 'Your email not exist. Please try again.');
        }
    }
}
