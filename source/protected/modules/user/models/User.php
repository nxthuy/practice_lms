<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $user_id
 * @property integer $user_role_id
 * @property integer $subscription_history_id
 * @property string $user_name
 * @property string $user_email
 * @property string $user_password
 * @property integer $user_password_expiry_time
 * @property string $user_activekey
 * @property integer $user_created_time
 * @property integer $user_login_time
 * @property integer $user_last_visit_time
 * @property string $user_status
 * @property integer $user_no_of_reminder
 *
 * The followings are the available model relations:
 * @property Employer[] $employers
 * @property EmployerUser[] $employerUsers
 * @property SponsorAndProRequest[] $sponsorAndProRequests
 * @property UserRole $userRole
 * @property UserProfile $userProfiles
 * @property Leaves $userLeavesRequests
 */
class User extends CActiveRecord
{
    public $user_photo;
    public $oldPassword;
    public $newPassword;
    public $verifyPassword;

    public $userEmail;

    public $accept;
    public $verifyCode;

    CONST ROLE_ADMIN = 1;
    CONST ROLE_MANAGER = 2;
    CONST ROLE_GENERAL = 3;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name, user_role_id, user_email, user_password, verifyPassword', 'required', 'on'=>'addEmployerUser'),
            array('user_name, user_email', 'required', 'on'=>'addEmployerUser'),
            array('user_password','ext.validators.EPasswordStrength', 'min'=>7,'on'=>'addEmployerUser'),
            //array('accept', 'compare', 'compareValue' => 1, 'on'=>'registration', 'message' => 'You should Agree term to use our service'),
            array('verifyPassword','compare', 'compareAttribute'=>'user_password','on'=>'addEmployerUser' ,'message' =>'Verify password does not match the password'),


            //array('newPassword, verifyPassword, oldPassword', 'required', 'on'=>'changePassword'),
            //array('newPassword','ext.validators.EPasswordStrength', 'min'=>7,'on'=>'changePassword'),
            //array('verifyPassword', 'compare', 'compareAttribute'=>'newPassword','on'=>'changePassword' ,'message' =>'Verify password does not match the password'),
            //array('oldPassword', 'validateOldPassword','on'=>'changePassword'),

            //array('user_password, user_password_expiry_time', 'required', 'on'=>'active'),

            //array('user_activekey', 'required', 'on'=>'recoverActiveKey'),

            //array('subscription_history_id', 'required', 'on'=>'productUpgradation'),

           // array('userEmail', 'required', 'on'=>array('recovery','withCaptcha')),
           // array('userEmail', 'validateEmailOrUser','on'=>array('recovery','withCaptcha')),
            // array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => 'withCaptcha'),

            array('user_name, user_email', 'unique'),
            array('user_email','email'),
			array('user_role_id, subscription_history_id ,user_password_expiry_time, user_created_time, user_login_time, user_last_visit_time, user_no_of_reminder', 'numerical', 'integerOnly'=>true),
			array('user_name, user_email', 'length', 'max'=>50),
			//array('user_password, user_activekey', 'length', 'max'=>128),
			//array('user_status', 'length', 'max'=>8),
			// @todo Please remove those attributes that should not be searched.
			array('user_id, user_role_id, user_name, user_email, user_password, user_password_expiry_time, user_activekey, user_created_time, user_login_time, user_last_visit_time, user_status, user_no_of_reminder', 'safe', 'on'=>'search'),
		);
	}


    public function validateEmailOrUser($attribute, $params)
    {
        if(!empty($this->userEmail))
        {

            if (strpos($this->user_email,"@"))
            {
                $record = User::model()->findByAttributes(array(
                    'user_email'=>$this->userEmail
                ));
            }
            else
            {
                $record = User::model()->findByAttributes(array(
                    'user_name'=>$this->userEmail
                ));
            }

            if($record === null)
            {
                Yii::app()->user->setState('attempts-login', Yii::app()->user->getState('attempts-login') + 1);
                $this->addError($attribute, 'Invalid Username or Email.');
            }
            else
            {
                $this->user_id = $record->user_id;
            }
        }
    }


    public function validateOldPassword($attribute, $params)
    {
        if(!empty($this->oldPassword))
        {
            $record = User::model()->findByAttributes(array(
                'user_id'=>Yii::app()->user->getId(),
                'user_password'=>UserModule::encrypting($this->oldPassword)
            ));
            if($record === null)
            {
                $this->addError($attribute, 'Incorrect existing password');
            }
        }
    }


    protected function beforeSave()
    {
        if($this->isNewRecord)
        {
            $this->user_password = UserModule::encrypting($this->user_password);
            $this->user_password_expiry_time = null;
            //$this->user_password_expiry_time = time()+(24*60*60);
            if($this->getScenario()=='addEmployerUser')
            {
                $this->user_status = 'Active';
            }
            else
            {
                $this->user_role_id = 10;
            }
        }
        elseif($this->getScenario()=='changePassword')
        {
            if(!empty($this->newPassword))
                $this->user_password = UserModule::encrypting($this->newPassword);
        }

        $this->user_last_visit_time = time();
        $this->user_activekey= uniqid(rand(999,9999));

        return parent::beforeSave();
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'employers' => array(self::HAS_MANY, 'Employer', 'user_id'),
			//'employerUsers' => array(self::HAS_MANY, 'EmployerUser', 'user_id'),
			//'sponsorAndProRequests' => array(self::HAS_MANY, 'SponsorAndProRequest', 'user_id'),
			'userRole' => array(self::BELONGS_TO, 'UserRole', array('user_role_id' => 'user_role_id')),
			'userProfiles' => array(self::BELONGS_TO, 'UserProfile', array('user_id'=>'user_id')),
            'userLeavesRequests' => array(self::HAS_ONE, 'Leave', 'requester_id'),
            //'userLeavesApproves' => array(self::HAS_ONE, 'Leaves', 'approver_id'),
			//'subscription' => array(self::BELONGS_TO, 'SubscriptionHistory', 'subscription_history_id'),
            'profile' => array(self::BELONGS_TO, 'UserProfile', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'userEmail' => 'User Or Email',
            'newPassword' => 'New Password',
            'oldPassword' => 'Current Password',
            'user_id' => 'User',
            'user_role_id' => 'User Role',
            'user_name' => 'Username',
            'user_email' => 'Email',
            'user_password' => 'Password',
            'user_password_expiry_time' => 'User Password Expiry Time',
            'user_activekey' => 'User Activekey',
            'user_created_time' => 'User Created Time',
            'user_login_time' => 'User Login Time',
            'user_last_visit_time' => 'User Last Visit Time',
            'user_status' => 'User Status',
            'user_no_of_reminder' => 'User No Of Reminder',
            'verifyPassword' => 'Verify Password',
            'accept' => 'I Agree the terms and conditions.',
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_role_id',$this->user_role_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('user_password',$this->user_password,true);
		$criteria->compare('user_password_expiry_time',$this->user_password_expiry_time);
		$criteria->compare('user_activekey',$this->user_activekey,true);
		$criteria->compare('user_created_time',$this->user_created_time);
		$criteria->compare('user_login_time',$this->user_login_time);
		$criteria->compare('user_last_visit_time',$this->user_last_visit_time);
		$criteria->compare('user_status',$this->user_status,true);
		$criteria->compare('user_no_of_reminder',$this->user_no_of_reminder);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getRoleName() {
        $role = isset($this->user_role_id) ? $this->user_role_id : '';
        if ($role == User::ROLE_ADMIN) {
            return $role = 'Admin';
        } elseif ($role == User::ROLE_MANAGER) {
            return $role = 'Manager';
        } else {
            return $role = 'General';
        }
        return $role;
    }

    public function getFreeMember()
    {
        $criteria = new CDbCriteria();
        $leaderMember = LeaderMember::model()->findAll();
        foreach ($leaderMember as $item) {
            $criteria->addCondition('user_id!=' . $item->member_id);
        }
        $criteria->addCondition('user_role_id=' . User::ROLE_GENERAL);
        $freeMemberUser = User::model()->findAll(array('condition' => $criteria->condition));
        return $freeMemberUser;
    }

    public function getMemberOfMe($id)
    {
        $criteria = new CDbCriteria();

        $leaderMemberModel = LeaderMember::model()->findAllByAttributes(array('leader_id' => $id));
        $notMemberModel = LeaderMember::model()->findAllByAttributes(array(), 'leader_id <> :leader_id', array(':leader_id' => $id));
        $freeMemberUser = $this->getFreeMember();

        $freeMemberId = array();
        foreach ($freeMemberUser as $item) {
            $freeMemberId[] = $item->user_id;
        }
        $notMemberId = array();
        foreach ($notMemberModel as $item) {
            $notMemberId[] = $item->member_id;
        }
        $mergeArr = array_merge($freeMemberId, $notMemberId);

        foreach ($mergeArr as $item) {
            $criteria->addCondition('user_id!=' . $item);
        }
        foreach ($leaderMemberModel as $item) {
            $criteria->addCondition('user_id=' . $item->member_id, 'OR');
        }
        $criteria->addCondition('user_role_id=' . User::ROLE_GENERAL);
        $memberUser = User::model()->findAll(array('condition' => $criteria->condition));

        return $memberUser;
    }

    /**
     * Use for Open HOAuth
     *
     * Returns User model by its email
     *
     * @param string $email
     * @access public
     * @return User
     */
    public function findByEmail($email) {
        return self::model()->findByAttributes(array('email' => $email));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
