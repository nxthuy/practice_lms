<?php
/**
 * Created by PhpStorm.
 * User: phat
 * Date: 10/28/2014
 * Time: 5:23 PM
 */

class LeaderMember extends CActiveRecord {
    public function tableName()
    {
        return 'leader_member';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
} 