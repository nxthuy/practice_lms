<?php

class RecoveryForm extends CFormModel
{
    public $password;
    public $repassword;
    /**
     * Declares the validation rules.
     * The rules state that both password and repassword need to be required.
     * Repassword match password
     * Length of password and repassword are 6 chars.
     */
    public function rules()
    {
        return array(
            // Both password and repassword need to be required.
            array('password, repassword', 'required'),
            // Repassword match password
            array('repassword', 'compare', 'compareAttribute'=>'password'),
            // Length of password and repassword are 6 chars.
            array('password, repassword', 'length', 'min'=>6),
        );
    }

}
