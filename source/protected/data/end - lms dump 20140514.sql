SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Annual', 'Anual Leave');
INSERT INTO `categories` VALUES ('2', 'Personal', 'Personal Leave');
INSERT INTO `categories` VALUES ('3', 'Wedding', 'Wedding Leave');
INSERT INTO `categories` VALUES ('4', 'Sick', 'Sick Leave');
INSERT INTO `categories` VALUES ('5', 'Materiny', 'Materiny Leave');

-- ----------------------------
-- Table structure for leaves
-- ----------------------------
DROP TABLE IF EXISTS `leaves`;
CREATE TABLE `leaves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `approver_id` int(10) unsigned NOT NULL,
  `requester_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `from_time` datetime NOT NULL,
  `to_time` datetime NOT NULL,
  `applied_days` float(8,2) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cc_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wol_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urgent_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of leaves
-- ----------------------------
INSERT INTO `leaves` VALUES ('1', '2', '1', '4', '2014-05-08 18:02:39', '2014-05-09 18:02:54', '1.00', 'My hand feel badly.', 'admin@ceresolutions.com', 'Alaska', '0936123435', 'Yes, goodluck to you.', '2', '2014-05-08 18:04:57', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('2', '1', '2', '3', '2014-05-09 01:54:04', '2014-05-10 01:54:07', '2.00', 'none.', 'root@ceresolutions.com', 'Hawaii', '0919534357', 'Reject', '3', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('3', '1', '2', '3', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick.', 'admin@ceresolutions.com', 'Home.', '0909090909', 'approve', '2', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('4', '1', '2', '2', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick again.', 'pdtran@ceresolutions.com', 'Binh Thanh.', '0919091909', 'Rejected', '3', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('5', '1', '2', '3', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick.', 'admin@ceresolutions.com', 'Home.', '0909090909', ' ', '1', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('6', '1', '2', '2', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick again.', 'pdtran@ceresolutions.com', 'Binh Thanh.', '0919091909', ' ', '1', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('7', '1', '2', '3', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick.', 'admin@ceresolutions.com', 'Home.', '0909090909', ' ', '1', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('8', '1', '2', '2', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick again.', 'pdtran@ceresolutions.com', 'Binh Thanh.', '0919091909', ' ', '1', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('9', '1', '2', '3', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick.', 'admin@ceresolutions.com', 'Home.', '0909090909', ' ', '1', '2014-05-09 01:55:06', '0000-00-00 00:00:00');
INSERT INTO `leaves` VALUES ('10', '1', '2', '2', '2014-05-09 01:54:04', '2014-05-09 01:54:04', '1.00', 'I\'m sick again.', 'pdtran@ceresolutions.com', 'Binh Thanh.', '0919091909', ' ', '1', '2014-05-09 01:55:06', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) DEFAULT NULL,
  `subscription_history_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_password_expiry_time` int(11) DEFAULT NULL,
  `user_activekey` varchar(128) NOT NULL,
  `user_created_time` int(11) DEFAULT NULL,
  `user_login_time` int(11) DEFAULT NULL,
  `user_last_visit_time` int(11) DEFAULT NULL,
  `user_status` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive',
  `user_no_of_reminder` int(2) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '0', 'admin', 'admin@ceresolutions.com', '32091411de9ceee58948b8712ae94dac4a338c11cfa28322833bc8a1c8ef13ee ', '1388779199', '228652cc55474dbde', '1387267274', null, '1389122887', 'Active', null);
INSERT INTO `user` VALUES ('2', '2', '0', 'nxthuy', 'nxthuy@ceresolutions.com', '32091411de9ceee58948b8712ae94dac4a338c11cfa28322833bc8a1c8ef13ee ', null, '1', null, null, null, 'Active', null);

-- ----------------------------
-- Table structure for user_profile
-- ----------------------------
DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE `user_profile` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_first_name` varchar(45) DEFAULT NULL,
  `user_last_name` varchar(45) DEFAULT NULL,
  `user_gender` enum('Unknown','Male','Female') NOT NULL,
  `user_birthday` date DEFAULT NULL,
  `user_phone` int(11) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  `user_photo` varchar(200) DEFAULT NULL,
  `user_dob` int(11) DEFAULT NULL,
  `user_signature` blob,
  `user_signature_type` varchar(25) NOT NULL,
  `user_signature_size` varchar(25) NOT NULL,
  `user_signature_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_profile
-- ----------------------------
INSERT INTO `user_profile` VALUES ('1', '1', 'Tran', 'Pham', 'Male', '1980-10-27', '919534357', 'Ung Van Khiem St., Binh Thanh Dst., Ho Chi Minh city', '/img/avatar1.jpg', null, 0x686868, 'image/jpeg', '5810', 'images.jpg');
INSERT INTO `user_profile` VALUES ('2', '2', 'Thuy', 'Nguyen', 'Male', '1987-04-14', '936123435', 'Cay Tram St., Go Vap Dst., Ho Chi Minh city', '/img/avatar2.jpg', null, null, 'image/jpeg', '5810', 'images.ipg');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) NOT NULL,
  `user_status` int(1) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', 'administrator', '1');
INSERT INTO `user_role` VALUES ('2', 'manager', '1');
INSERT INTO `user_role` VALUES ('3', 'leader', '1');
