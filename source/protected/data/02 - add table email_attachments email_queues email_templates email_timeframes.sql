-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2014 at 05:16 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_attachments`
--

CREATE TABLE IF NOT EXISTS `email_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queue_id` int(11) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_queues`
--

CREATE TABLE IF NOT EXISTS `email_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `from_name` varchar(50) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `to_name` text NOT NULL,
  `to_email` text NOT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_time` int(10) NOT NULL,
  `send_time` int(10) NOT NULL,
  `sent_time` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `email_queues`
--

INSERT INTO `email_queues` (`id`, `template_id`, `type`, `from_name`, `from_email`, `to_name`, `to_email`, `reply_to`, `subject`, `content`, `created_time`, `send_time`, `sent_time`, `status`) VALUES
(1, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413199191, 1413199491, NULL, 0),
(2, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413199250, 1413199550, NULL, 0),
(3, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413199279, 1413199579, 1413199281, 2),
(4, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413251610, 1413251910, NULL, 3),
(5, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413251698, 1413251998, NULL, 3),
(6, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413252269, 1413252569, NULL, 3),
(7, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413252346, 1413252646, NULL, 3),
(8, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413252465, 1413252765, NULL, 3),
(9, 2, 1, 'Akoora', 'contact@akoora.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@akoora.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413252630, 1413252930, 1413252633, 2),
(10, 2, 1, 'Lms', 'contact@lms.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@lms.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413252983, 1413253283, 1413252986, 2),
(11, 2, 1, 'Lms', 'contact@lms.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@lms.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413253018, 1413253318, 1413253025, 2),
(12, 2, 1, 'Lms', 'contact@lms.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@lms.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Forgot Password</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<br>\nYour email was successfully sent. <br>\n<br>\n\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413253816, 1413254116, 1413253820, 2),
(13, 2, 1, 'Lms', 'contact@lms.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@lms.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;">\n			<h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:34px;font-weight:normal;margin-top:20px;padding-left:8px;">\n				CERES LMS - Reset Password\n			</h1>\n		</td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;">\n			<div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n				Your password at CERES LMS has been reset. You may log in using the password listed below.<br />\n				<b>[''RANDOM_PASS'']</b>\n			</div>\n		</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413255111, 1413255411, 1413255116, 2),
(14, 2, 1, 'Lms', 'contact@lms.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@lms.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;">\n			<h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:34px;font-weight:normal;margin-top:20px;padding-left:8px;">\n				CERES LMS - Reset Password\n			</h1>\n		</td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;">\n			<div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n				Your password at CERES LMS has been reset. You may log in using the password listed below.<br />\n				<b>["RANDOM_PASS"]</b>\n			</div>\n		</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413255218, 1413255518, 1413255222, 2),
(15, 2, 1, 'Lms', 'contact@lms.com', 'jikhk', 'nhphat@ceresolutions.com', 'contact@lms.com', 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;">\n			<h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:34px;font-weight:normal;margin-top:20px;padding-left:8px;">\n				CERES LMS - Reset Password\n			</h1>\n		</td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;">\n			<div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n				Your password at CERES LMS has been reset. You may log in using the password listed below.<br />\n				<b>vqIVmYNO4o</b>\n			</div>\n		</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1413255296, 1413255596, 1413255300, 2);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeframe_id` int(11) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `from_name` varchar(50) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `to_name` text NOT NULL,
  `to_email` text NOT NULL,
  `reply_to` varchar(10) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `resend_if_fail` tinyint(1) NOT NULL DEFAULT '1',
  `delete_if_success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `timeframe_id`, `desc`, `from_name`, `from_email`, `to_name`, `to_email`, `reply_to`, `cc`, `bcc`, `subject`, `content`, `resend_if_fail`, `delete_if_success`) VALUES
(1, 1, 'User sign up new account', '(auto)', '(auto)', '(auto)', '(auto)', '(auto)', NULL, NULL, 'Chalksy - Sign up', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;"><h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Sign up</h1>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo.png" id="logo" alt="logo" style="width: 120px; float: right; margin-left: 60px; cursor: default;"></a></td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;"><div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n<p class="current" style="padding:14px;color:#222222;line-height:18px;"> Hello,<br>\n<br>\n Welcome to <a href="http://chalksy.com/" target="_blank" style="color:#7d003f;">Chalksy</a>. The innovative new auction management system. <br>\n<br>\n Thank you for registering! Please click on the link below to verify your account: <br>\n<a class="current" href="[ACTIVATE_URL]">[ACTIVATE_URL]</a><br>\n<br>\n Sincerely,<br>\n Chalksy team </p>\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;" cellpadding="0" cellspacing="5" width="100%">\n<tbody>\n	<tr>\n		<td><div style="margin-right:auto;margin-left:auto;width:600px;">\n<p style="width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;"><br>\n If you are have any questions, please feel free to contact us at <a style="color:#fff" href="mailto:contact@chalksy.com">contact@chalksy.com</a> This email is a post only email. Replies to this email are not monitored or answered. </p>\n<a href="http://chalksy.com"><img src="http://chalksy.com/images/logo-bt.png" alt="logo" style="width: 100px; float: right; margin-left: 60px; cursor: default;"></a></div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1, 0),
(2, 2, 'Forgot password', '', '', '', '', NULL, NULL, NULL, 'Forgot Password', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\n<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellpadding="0" cellspacing="5">\n<tbody>\n	<tr style="background:#DDDCE2">\n		<td><img src="http://chalksy.com/images/spacer.gif" alt="" height="12" width="1"></td>\n	</tr>\n\n	<tr>\n		<td style="padding:14px;">\n			<h1 class="current" style="width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:34px;font-weight:normal;margin-top:20px;padding-left:8px;">\n				CERES LMS - Password Reset\n			</h1>\n		</td>\n	</tr>\n\n	<tr>\n		<td style="padding:10px;">\n			<div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\n				Your password at CERES LMS has been reset. You may log in using the password listed below.<br />\n				<b>[RANDOM_PASS]</b>\n			</div>\n		</td>\n	</tr>\n</tbody>\n</table>\n</div>', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_timeframes`
--

CREATE TABLE IF NOT EXISTS `email_timeframes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(100) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `email_timeframes`
--

INSERT INTO `email_timeframes` (`id`, `desc`, `value`) VALUES
(1, 'Immediately', 1),
(2, 'Within 5 minutes', 5),
(3, 'Within 10 minutes', 10),
(4, 'Within 15 minutes', 15),
(5, 'Within 30 minutes', 30),
(6, 'Within 1 hour', 60),
(7, 'Within 2 hours', 120),
(8, 'Within 5 hours', 300);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
