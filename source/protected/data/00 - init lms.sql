/*
SQLyog Ultimate v9.51 
MySQL - 5.5.27 : Database - lms
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`name`,`description`) values (1,'Annual','Anual Leave');
insert  into `categories`(`id`,`name`,`description`) values (2,'Personal','Personal Leave');
insert  into `categories`(`id`,`name`,`description`) values (3,'Wedding','Wedding Leave');

/*Table structure for table `leaves` */

DROP TABLE IF EXISTS `leaves`;

CREATE TABLE `leaves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `approver_id` int(10) unsigned NOT NULL,
  `recommend_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `from_time` datetime NOT NULL,
  `to_time` datetime NOT NULL,
  `applied_days` float(8,2) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cc_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urgent_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `leaves` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) DEFAULT NULL,
  `subscription_history_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_password_expiry_time` int(11) DEFAULT NULL,
  `user_activekey` varchar(128) NOT NULL,
  `user_created_time` int(11) DEFAULT NULL,
  `user_login_time` int(11) DEFAULT NULL,
  `user_last_visit_time` int(11) DEFAULT NULL,
  `user_status` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive',
  `user_no_of_reminder` int(2) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_role_id`,`subscription_history_id`,`user_name`,`user_email`,`user_password`,`user_password_expiry_time`,`user_activekey`,`user_created_time`,`user_login_time`,`user_last_visit_time`,`user_status`,`user_no_of_reminder`) values (1,1,0,'admin','admin@ceresolutions.com','32091411de9ceee58948b8712ae94dac4a338c11cfa28322833bc8a1c8ef13ee ',1388779199,'228652cc55474dbde',1387267274,NULL,1389122887,'Active',NULL);

/*Table structure for table `user_profile` */

DROP TABLE IF EXISTS `user_profile`;

CREATE TABLE `user_profile` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_first_name` varchar(45) DEFAULT NULL,
  `user_last_name` varchar(45) DEFAULT NULL,
  `user_photo` varchar(200) DEFAULT NULL,
  `user_dob` int(11) DEFAULT NULL,
  `user_signature` blob,
  `user_signature_type` varchar(25) NOT NULL,
  `user_signature_size` varchar(25) NOT NULL,
  `user_signature_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user_profile` */

insert  into `user_profile`(`user_profile_id`,`user_id`,`user_first_name`,`user_last_name`,`user_photo`,`user_dob`,`user_signature`,`user_signature_type`,`user_signature_size`,`user_signature_name`) values (1,1,'asm','hossain','sajib-6052.jpg',NULL,'hhh','image/jpeg','5810','images.jpg');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) NOT NULL,
  `user_status` int(1) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`user_role_id`,`user_role_name`,`user_status`) values (1,'administrator',1);
insert  into `user_role`(`user_role_id`,`user_role_name`,`user_status`) values (2,'manager',1);
insert  into `user_role`(`user_role_id`,`user_role_name`,`user_status`) values (3,'leader',1);
insert  into `user_role`(`user_role_id`,`user_role_name`,`user_status`) values (4,'employee',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
