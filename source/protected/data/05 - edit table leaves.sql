/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : lms_db

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2014-10-22 10:28:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for leaves
-- ----------------------------
DROP TABLE IF EXISTS `leaves`;
CREATE TABLE `leaves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `approver_id` int(10) unsigned NOT NULL,
  `requester_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `from_time` datetime NOT NULL,
  `to_time` datetime NOT NULL,
  `applied_days` float(8,2) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cc_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wol_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urgent_phone` int(11) NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `four_digit_year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
