ALTER TABLE `leaves` ADD COLUMN `requester_id`  int(10) UNSIGNED NOT NULL AFTER `approver_id`;
ALTER TABLE `leaves` ADD COLUMN `wol_address`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `cc_email`;
ALTER TABLE `leaves` DROP COLUMN `recommend_id`;

ALTER TABLE `user_profile` ADD COLUMN `user_gender`  enum('Unknown','Male','Female') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `user_last_name`;
ALTER TABLE `user_profile` ADD COLUMN `user_birthday`  date NULL DEFAULT NULL AFTER `user_gender`;
ALTER TABLE `user_profile` ADD COLUMN `user_phone`  int(11) NULL DEFAULT NULL AFTER `user_birthday`;
ALTER TABLE `user_profile` ADD COLUMN `user_address`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `user_phone`;
