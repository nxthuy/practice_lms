<thead>
<tr>
    <th>Type</th>
    <th>Type Leave</th>
    <th>Leave has</th>
    <th>Leave</th>
    <th>Left</th>
    <th>No. of Days</th>
</tr>
</thead>
<tbody>
<tr>
    <td rowspan="3">Company pay leave (days)</td>
    <td>Annual leave</td>
    <td class="t-center">
        <?php
        echo app()->Params['annual'];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[0];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo app()->Params['annual'] - $leaveSummariesByUser[0];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[0];
        ?>
    </td>
</tr>
<tr>
    <td>Personal</td>
    <td class="t-center">
        <?php
        echo app()->Params['personal'];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[1];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo app()->Params['personal'] - $leaveSummariesByUser[1];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[1];
        ?>
    </td>
</tr>
<tr>
    <td>Wedding</td>
    <td class="t-center">
        <?php
        echo app()->Params['wedding'];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[2];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo app()->Params['wedding'] - $leaveSummariesByUser[2];
        ?>
    </td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[2];
        ?>
    </td>
</tr>
<tr>
    <td rowspan="2">Social insurance pay leave (70%)</td>
    <td>Sick</td>
    <td>&#32;</td>
    <td>
        <?php
        echo $leaveSummariesByUser[3];
        ?>
    </td>
    <td>&#32;</td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[3];
        ?>
    </td>
</tr>
<tr>
    <td>Maternity</td>
    <td>&#32;</td>
    <td>
        <?php
        echo $leaveSummariesByUser[4];
        ?>
    </td>
    <td>&#32;</td>
    <td class="t-center">
        <?php
        echo $leaveSummariesByUser[4];
        ?>
    </td>
</tr>
</tbody>
<tfoot>
<tr>
    <td colspan="5">Total</td>
    <td class="t-center">
        <?php
        echo array_sum($leaveSummariesByUser);
        ?>
    </td>
</tr>
</tfoot>