<?php

/**
 * This is the model class for table "article".
 *
 * The followings are the available columns in table 'article':
 * @property integer $article_id
 * @property integer $article_category_id
 * @property integer $user_id
 * @property string $article_title
 * @property string $article_body
 * @property string $tags
 * @property integer $featured
 * @property integer $article_created_time
 * @property integer $article_updated_time
 * @property string $article_status
 *
 * The followings are the available model relations:
 * @property ArticalCategory $articleCategory
 * @property User $user
 * @property ArticleComment[] $articleComments
 */
class Article extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('article_category_id,article_body, article_title ', 'required', 'on'=>'çreate'),
			array('article_category_id, user_id, featured, article_created_time, article_updated_time', 'numerical', 'integerOnly'=>true),
			array('article_title', 'length', 'max'=>250),
			array('tags', 'length', 'max'=>200),
			array('article_status', 'length', 'max'=>8),
			array('article_body', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('article_id, article_category_id, user_id, article_title, article_body, tags, featured, article_created_time, article_updated_time, article_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articleCategory' => array(self::BELONGS_TO, 'ArticalCategory', 'article_category_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'articleComments' => array(self::HAS_MANY, 'ArticleComment', 'article_id'),
            'commentCount'=>array(self::STAT, 'ArticleComment', 'article_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'article_id' => 'Article',
			'article_category_id' => 'Category',
			'user_id' => 'User',
			'article_title' => 'Title',
			'article_body' => 'Body',
			'tags' => 'Tags',
			'featured' => 'Featured',
			'article_created_time' => 'Article Created Time',
			'article_updated_time' => 'Article Updated Time',
			'article_status' => 'Article Status',
		);
	}

//    protected function beforeSave()
//    {
//        if($this->isNewRecord)
//        {
//            $this->featured = 0;
//            $this->user_id = Yii::app()->user->getId();
//            $this->article_created_time = time();
//            $this->article_updated_time = time();
//        }
//
//        return parent::beforeSave();
//    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('article_id',$this->article_id);
		$criteria->compare('article_category_id',$this->article_category_id);
		$criteria->compare('user_id',Yii::app()->user->getId());
		$criteria->compare('article_title',$this->article_title,true);
		$criteria->compare('article_body',$this->article_body,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('featured',$this->featured);
		$criteria->compare('article_created_time',$this->article_created_time);
		$criteria->compare('article_updated_time',$this->article_updated_time);
		$criteria->compare('article_status',$this->article_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
