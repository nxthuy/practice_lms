<?php

/**
 * This is the model class for table "leaves".
 *
 * The followings are the available columns in table 'leaves':
 * @property string $id
 * @property string $approver_id
 * @property string $requester_id
 * @property string $category_id
 * @property string $from_time
 * @property string $to_time
 * @property double $applied_days
 * @property string $reason
 * @property string $cc_email
 * @property string $wol_address
 * @property string $urgent_phone
 * @property string $notes
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property User $userRequest
 * @property User $userApprove
 * @property UserProfile $userApproveProfiles
 * @property UserProfile $userRequestProfiles
 * @property Categories $category
 */
class Leave extends CActiveRecord
{
    public $userprofile_search;

    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REJECTED = 3;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'leaves';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        //$user_first_name;
		return array(
			array('from_time, to_time', 'required','on'=>'createleave'),
            array('from_time, to_time', 'authenticateTime'),
            array('status', 'numerical', 'integerOnly'=>true),
			array('applied_days', 'numerical'),
			array('approver_id, requester_id, category_id', 'length', 'max'=>10),
			array('reason, cc_email, wol_address, urgent_phone', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),

			array('id, approver_id, requester_id, category_id, from_time, to_time, applied_days, reason, cc_email, wol_address, urgent_phone, notes, status, created_at, updated_at, userprofile_search', 'safe', 'on'=>'search'),
            array('id, approver_id, requester_id, category_id, from_time, to_time, applied_days, reason, cc_email, wol_address, urgent_phone, notes, status, created_at, updated_at, userprofile_search', 'safe', 'on'=>'summary'),
		);
	}

    public function authenticateTime() {
        $fromTime = date_format(date_create($this->from_time), 'Y-m-d H:i:s');
        $toTime = date_format(date_create($this->to_time), 'Y-m-d H:i:s');
        if (strtotime($fromTime) < time()) {
            $this->addError('from_time', 'From-Time invalid');
        } elseif (strtotime($toTime) <= strtotime($fromTime)) {
            $this->addError('to_time', 'To-Time invalid');
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'userRequest' => array(self::BELONGS_TO, 'User', 'requester_id'),
            'userApprove' => array(self::BELONGS_TO, 'User', 'approver_id'),
            'userApproverProfiles' => array(self::HAS_ONE, 'UserProfile', array('user_id' => 'approver_id')),
            'userRequestProfiles' => array(self::HAS_ONE, 'UserProfile', array('user_id' => 'requester_id')),
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'userProfiles' => array(self::BELONGS_TO, 'UserProfile', 'approver_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'approver_id' => 'Approving Officer',
			'requester_id' => 'Requester',
			'category_id' => 'Leave Type',
			'from_time' => 'From',
			'to_time' => 'To',
			'applied_days' => 'Working Days Applied',
			'reason' => 'Remark (Max 200 chars)',
			'cc_email' => 'CC to other',
            'wol_address' => 'Address While On Leave',
			'urgent_phone' => 'Urgent Phone',
			'notes' => 'Notes',
			'status' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

        $criteria->with = array('userProfiles');

		//$criteria->compare('id',$this->id);
		$criteria->compare('userProfiles.user_first_name',$this->userprofile_search, true);
		$criteria->compare('requester_id',$this->requester_id,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('from_time',$this->from_time, true);
		$criteria->compare('to_time',$this->to_time,true);
		$criteria->compare('applied_days',$this->applied_days);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('cc_email',$this->cc_email,true);
        $criteria->compare('wol_address',$this->wol_address,true);
		$criteria->compare('urgent_phone',$this->urgent_phone,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

        $nowYear = CTimestamp::get4DigitYear(date('y',time()));
        $criteria->addCondition('four_digit_year='.$nowYear);
        $criteria->addCondition('requester_id='.user()->id);

        $pageSize = app()->getParams()['pageSize'];
        if (app()->request->isAjaxRequest) {
            if (app()->request->getParam('ajaxPageSize') != NULL) {
                $pageSize = app()->request->getParam('ajaxPageSize');
            }
        }

		return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort'=>array(
                'defaultOrder'=>'created_at DESC',
                'attributes'=>array(
                    'userprofile_search'=>array(
                        'asc'=>'userProfiles.user_first_name',
                        'desc'=>'userProfiles.user_first_name DESC',
                    ),
                    '*',
                ),
            ),
            'pagination' => array(
                'pageSize'=> $pageSize
            ),
		));
	}

    public function getPendingLeaveCount()
    {   if (isset(user()->role)) {
            if (user()->role == User::ROLE_GENERAL) {
                $pendingLeaveCount = $this->count('status=' .self::STATUS_PENDING. ' AND requester_id=' .user()->id);
            } elseif (user()->role == User::ROLE_MANAGER) {
                $leaderMember = LeaderMember::model()->findAllByAttributes(array(
                    'leader_id' => user()->id,
                ));
                $pendingLeaveCount = $this->count('status=' .self::STATUS_PENDING. ' AND requester_id=' .user()->id);
                foreach ($leaderMember as $item) {
                    $pendingLeaveCount += $this->count('status=' .self::STATUS_PENDING. ' AND requester_id=' .$item->member_id);
                }
            } else {
                $pendingLeaveCount = $this->count('status=' .self::STATUS_PENDING);
            }
    }
        return $pendingLeaveCount;
    }
/*
    public function approve()
    {
        $this->status = Leaves::STATUS_APPROVED;
        $this->update(array('status'));
    }

    public function reject()
    {
        $this->status = Leaves::STATUS_REJECTED;
        $this->update(array('status'));
    }

    public function message()
    {
       $this->notes = $_POST['notes'];
        $this->update(array('notes'));
    }
*/
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Leaves the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getListCategories(){
        $categories = Category::model()->findAll();
        return CHtml::listData($categories,'id', 'name');
    }

    public function getFromDate() {
        $fromTime = date_create($this->from_time);
        return $this->from_time = date_format($fromTime, 'd/m/Y');
    }

    public function getToDate() {
        $toTime = date_create($this->to_time);
        return $this->to_time = date_format($toTime, 'd/m/Y');
    }

    public function getNameRequester() {
        $userProfile = UserProfile::model()->findByAttributes(array('user_id' => $this->requester_id));
        $firstName = $userProfile->user_first_name;
        $lastName = $userProfile->user_last_name;
        $nameRequester = $firstName . ' ' . $lastName;
        return $nameRequester;
    }

    public function getNameApprover() {
        $firstName = $this->userProfiles->user_first_name;
        $lastName = $this->userProfiles->user_last_name;
        $nameApprover = $firstName . ' ' . $lastName;
        return $nameApprover;
    }


    public function getLeaveSummariesByUser($userId) {
        $sum = array();
        for ($i=1; $i<=5; $i++) {
            $sum[$i-1] = Yii::app()->db->createCommand("SELECT SUM(`applied_days`) FROM `leaves` WHERE `requester_id` =" . $userId . " AND `four_digit_year` =" . CTimestamp::get4DigitYear(date('y', time())) . " AND `status` =" .'2'. " AND `category_id` =" . $i)->queryScalar();
        }
        return $sum;
    }
}
