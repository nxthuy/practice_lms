$(".datepicker").datepicker();
$("#Leaves_to_time").datepicker({
}).on('changeDate', function (selected) {
    d1=$("#Leaves_from_time").val();
    d2=$("#Leaves_to_time").val();
    var start   =   new Date(d1);
    var end =   new Date(d2);
    var holidayArr  =   [

    ];
    var countLeave=0;
    while(start <= end){
        var i;
        var checkHolidayDay=0;
        for (i = 0; i < holidayArr.length; ++i) {
            var holiday   =   new Date(holidayArr[i]);
            if(holiday.getTime() === start.getTime()){
                checkHolidayDay=1;break;
            }

        }
        if(start.getDay()!=0&&start.getDay()!=6&&checkHolidayDay==0){
            countLeave+=1;
        }
        var newDate = start.setDate(start.getDate() + 1);
        start = new Date(newDate);
    }
    $("#Leaves_applied_days").val(countLeave);
});
